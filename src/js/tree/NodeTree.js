import TreeNode from "./TreeNode";

export default class NodeTree {
    constructor(rootElement) {
        this.counter = 0;
        this.root = new TreeNode(rootElement, this.counter++);
        this.root.name = "Sketch";
        this.eleToNode = new Map();
        this.eleToNode.set(rootElement, this.root);
    }

    addElement(element, parentEle) {
        const parentNode = this.eleToNode.get(parentEle) || this.root;
        const eleNodeId = this.counter;
        const eleNode = new TreeNode(element, eleNodeId, parentNode);
        this.counter++;
        parentNode.children.push(eleNode);
        this.eleToNode.set(element, eleNode);
        return eleNodeId;
    }

    removeElement(element) {
        const eleNode = this.eleToNode.get(element);
        const parentNode = eleNode.parent;
        if (eleNode.children !== undefined && eleNode.children.length > 0) {
            eleNode.children.forEach(child => {
                this.moveElement(child.element, parentNode.element);
            });
        }
        this.removeElementFromArray(eleNode, parentNode.children);
        this.eleToNode.delete(element);
    }

    moveElement(element, parentEle) {
        const eleNode = this.eleToNode.get(element);
        const oldParentNode = eleNode.parent;
        this.removeElementFromArray(eleNode, oldParentNode.children);
        const newParentNode = this.eleToNode.get(parentEle);
        newParentNode.children.push(eleNode);
        eleNode.parent = newParentNode;
    }

    getId(element) {
        if (!this.containsElement(element)) {
            console.log("Element ", element, " not in tree! Cannot get id.");
            console.trace();
            return null;
        }
        return this.eleToNode.get(element).id;
    }

    getName(element) {
        if (!this.containsElement(element)) {
            console.log("Element ", element, " not in tree! Cannot get name.");
            console.trace();
            return null;
        }
        return this.eleToNode.get(element).name;
    }

    getElementById(id) {
        const node = Array.from(this.eleToNode.values())
            .find(n => n.id.toString() === id);

        if (node === undefined) {
            console.log("There is no element with id ", id, " in the tree!");
            return null;
        }

        return node.element;
    }

    getParentElement(element) {
        return this.eleToNode.get(element).parent.element;
    }

    getChildElements(element) {
        const childNodes = this.eleToNode.get(element).children;
        return childNodes.map(n => n.element);
    }

    getAllElements() {
        return this.eleToNode.keys();
    }

    renameNode(element, name) {
        this.eleToNode.get(element).name = name;
    }

    containsElement(element) {
        return this.eleToNode.get(element) !== undefined;
    }

    removeElementFromArray(element, array) {
        const index = array.indexOf(element);
        if (index > -1) {
            array.splice(index, 1);
        }
    }
}
