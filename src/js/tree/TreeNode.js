export default class TreeNode {
    constructor(element, id, parent = null, children = null) {
        this.element = element;
        this.id = id;
        this.name = "element-" + this.id;
        this.parent = parent;
        this.children = children || [];
    }
}
