export class SvgElementCreated extends Event {
    constructor(instance) {
        super(SvgElementCreated.name, { bubbles: true, cancelable: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-created";
    }
}

export class SvgElementSelectedInHierarchy extends Event {
    constructor(instance) {
        super(SvgElementSelectedInHierarchy.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-selected-in-hierarchy";
    }
}

export class SvgElementSelected extends Event {
    constructor(instance) {
        super(SvgElementSelected.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-selected";
    }
}

export class SvgElementDeselected extends Event {
    constructor(instance) {
        super(SvgElementDeselected.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-deselected";
    }
}

export class SvgElementMoved extends Event {
    constructor(instance) {
        super(SvgElementMoved.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-moved";
    }
}

export class SvgElementParentChanged extends Event {
    constructor(instance, newParent) {
        super(SvgElementParentChanged.name, { bubbles: true });
        this.instance = instance;
        this.newParent = newParent;
    }

    static get name() {
        return "svg-element-parent-changed";
    }
}

export class SvgElementDeleted extends Event {
    constructor(instance) {
        super(SvgElementDeleted.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-deleted";
    }
}

export class SvgElementMetadataChanged extends Event {
    constructor(instance) {
        super(SvgElementMetadataChanged.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-metadata-changed";
    }
}

export class SvgElementParentScaled extends Event {
    constructor(instance) {
        super(SvgElementParentScaled.name, { bubbles: true });
        this.instance = instance;
    }

    static get name() {
        return "svg-element-parent-scaled";
    }
}
