export class HierarchyNodeSelected extends Event {
    constructor(id) {
        super(HierarchyNodeSelected.name, { bubbles: true });
        this.id = id;
    }

    static get name() {
        return "hierarchy-node-selected";
    }
}

export class HierarchyNodeMoved extends Event {
    constructor(id, parentId) {
        super(HierarchyNodeMoved.name, { bubbles: true });
        this.id = id;
        this.parentId = parentId;
    }

    static get name() {
        return "hierarchy-node-moved";
    }
}

export class HierarchyNodeRenamed extends Event {
    constructor(id, newName) {
        super(HierarchyNodeRenamed.name, { bubbles: true });
        this.id = id;
        this.newName = newName;
    }

    static get name() {
        return "hierarchy-node-renamed";
    }
}

export class HierarchyNodeDeleted extends Event {
    constructor(id) {
        super(HierarchyNodeDeleted.name, { bubbles: true });
        this.id = id;
    }

    static get name() {
        return "hierarchy-node-deleted";
    }
}
