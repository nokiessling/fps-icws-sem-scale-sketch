export default class Vector2D {
    constructor(x, y) {
        this.x = x;
        this.y = y;

        this.isNormalized = false;
    }

    magnitude() {
        return Math.sqrt((this.x ** 2) + (this.y ** 2));
    }

    normalize() {
        if (this.isNormalized) return;

        const magnitude = this.magnitude();
        this.x /= magnitude;
        this.y /= magnitude;

        this.isNormalized = true;
    }

    slope() {
        return this.y / this.x;
    }

    slopeAngle() {
        const angle = this.toDegrees(Math.atan(this.slope()));
        return this.x < 0 ? angle + 180 : angle;
    }

    toDegrees(angle) {
        return angle * (180 / Math.PI);
    }
}
