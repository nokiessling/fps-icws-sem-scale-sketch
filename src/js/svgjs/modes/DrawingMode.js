import Mode from "./Mode";
import { SvgElementCreated } from "../../events/SvgElementEvents";
import ListenerHandler from "../../helper/ListenerHandler";
import ToolManager from "../tools/ToolManager";

export default class DrawingMode extends Mode {
    constructor(toolsSidebarId) {
        super();
        this.htmlToolsSidebarId = toolsSidebarId;

        this.listenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );
    }

    activate() {
        super.activate();

        this.listenerHandler.on(SvgElementCreated.name, evt => {
            evt.instance.geoScalable();
            evt.instance.moveWithParentOnDrag();
        });
        ToolManager.Instance.activate();
        document.querySelector(this.htmlToolsSidebarId)
            .classList.remove("d-none");
    }

    deactivate() {
        super.deactivate();

        this.listenerHandler.removeAllListeners();
        ToolManager.Instance.deactivate();
        document.querySelector(this.htmlToolsSidebarId)
            .classList.add("d-none");
    }
}
