export default class ModeManager {
    constructor(modes, defaultModeName) {
        this.modes = new Map(modes);
        this.defaultModeName = defaultModeName;
        this.selectedModeName = null;
        this.selectedMode = null;
        this.selectMode(defaultModeName);
    }

    reset() {
        this.selectMode(this.defaultModeName);
    }

    selectMode(modeName) {
        if (this.selectedMode) {
            this.selectedMode.deactivate();
        }
        const mode = this.getModeByName(modeName);
        if (mode) {
            this.selectedModeName = modeName;
            this.selectedMode = mode;
            this.selectedMode.activate();
        }
        else {
            console.log("ModeManager: Could not select mode with name ",
                modeName, "! No such mode known.");
        }
    }

    getModeByName(modeName) {
        return this.modes.get(modeName);
    }
}
