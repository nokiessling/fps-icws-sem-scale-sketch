import Mode from "./Mode";
import {
    SvgElementDeleted,
    SvgElementSelected
} from "../../events/SvgElementEvents";
import ListenerHandler from "../../helper/ListenerHandler";
import SvgSelectionManager from "../selection/SvgSelectionManager";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";
import { deleteColor } from "../../utils/Colors";

export default class DeletingMode extends Mode {
    constructor() {
        super();

        this.listenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );
    }

    activate() {
        super.activate();

        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );

        SvgSelectionManager.Instance.activateInteractableSelection(
            existingElements, false, false, deleteColor
        );

        SvgSelectionManager.Instance.deselectElement(
            SvgSelectionManager.Instance.selectedElement
        );

        this.listenerHandler.on(SvgElementSelected.name, evt => {
            this.deleteSvgElement(evt.instance);
        });
    }

    deactivate() {
        super.deactivate();

        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );

        SvgSelectionManager.Instance
            .deactivateInteractableSelection(existingElements);

        this.listenerHandler.removeAllListeners();
    }

    deleteSvgElement(element) {
        element.node.dispatchEvent(new SvgElementDeleted(element));
        element.remove();
    }
}
