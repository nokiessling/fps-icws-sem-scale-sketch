import Mode from "./Mode";
import SvgSelectionManager from "../selection/SvgSelectionManager";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";
import ToolManager from "../tools/ToolManager";

export default class ScalingMode extends Mode {
    constructor(popupMenu) {
        super();

        this.popupMenu = popupMenu;
    }

    activate() {
        super.activate();

        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );

        SvgSelectionManager.Instance.activateInteractableSelection(
            existingElements, true, true
        );
    }

    deactivate() {
        super.deactivate();

        // deactivate tool manager in case mode was switched while placing
        // repeat path or custom dir
        if (ToolManager.Instance.active) {
            ToolManager.Instance.deactivate();
        }
        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );
        SvgSelectionManager.Instance
            .deactivateInteractableSelection(existingElements);
    }
}
