import Mode from "./Mode";
import ListenerHandler from "../../helper/ListenerHandler";
import SvgSelectionManager from "../selection/SvgSelectionManager";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";
import ToolManager from "../tools/ToolManager";

export default class SelectingMode extends Mode {
    constructor(popupMenu) {
        super();

        this.popupMenu = popupMenu;

        this.listenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );
    }

    activate() {
        super.activate();

        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );

        SvgSelectionManager.Instance
            .activateInteractableSelection(existingElements);
        this.popupMenu.activate(SvgSelectionManager.Instance.selectedElement);
    }

    deactivate() {
        super.deactivate();

        // deactivate tool manager in case mode was switched while placing
        // repeat path or custom dir
        if (ToolManager.Instance.active) {
            ToolManager.Instance.deactivate();
        }
        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );
        SvgSelectionManager.Instance
            .deactivateInteractableSelection(existingElements);
        this.popupMenu.deactivate();

        this.listenerHandler.removeAllListeners();
        // TODO: remove listeners
    }
}
