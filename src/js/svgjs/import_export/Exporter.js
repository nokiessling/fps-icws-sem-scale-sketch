import { SVG } from "@svgdotjs/svg.js";

export default class Exporter {
    constructor() {
        document.querySelector("#toolbarExport")
            .addEventListener("click", evt => {
                this.export();
            });
    }

    export() {
        // remove selection svg elements
        const svgEle = document.querySelector("svg");
        if (!svgEle) {
            console.log("There is nothing to export!");
            return;
        }
        const exportString = SVG(svgEle).svg(svgNode => {
            if (svgNode.node.classList !== undefined
                && (svgNode.node.classList.contains("selection-highlighting")
                || svgNode.node.classList.contains("selectable-clone"))) {
                return false;
            }

            if (svgNode.type === "svg" || svgNode.type === "use") {
                return svgNode;
            }

            const isSketchPart = svgNode.data("sketchPart");
            if (isSketchPart) {
                return svgNode;
            }

            return false;
        });

        // export
        const element = document.createElement("a");
        element.setAttribute(
            "href",
            "data:text/plain;charset=utf-8," + encodeURIComponent(exportString)
        );
        element.setAttribute("download", "drawing.svg");
        element.style.display = "none";

        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
}
