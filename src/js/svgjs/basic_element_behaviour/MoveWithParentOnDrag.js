import { extend, Element, Svg } from "@svgdotjs/svg.js";
import ListenerHandler from "../../helper/ListenerHandler";
import { SvgElementParentChanged } from "../../events/SvgElementEvents";
import ObserverHandler from "../../helper/ObserverHandler";
import Vector2D from "../../math/Vector2D";

class MoveWithParentOnDragHandler {
    constructor(el) {
        el.remember("_move_with_parent_on_drag", this);
        this.enabled = false;
        this.el = el;
        this.semParent = null;
        this.prevParentWidth = null;
        this.prevParentHeight = null;
        this.prevParentPos = null;
        this.xOffsetToParent = null;
        this.yOffsetToParent = null;
        this.elObserverHandler = new ObserverHandler(
            this.el,
            () => {
                if (this.semParent
                    && this.semParent.x() === this.prevParentPos.x
                    && this.semParent.y() === this.prevParentPos.y) {
                    this.calcElToParentOffsets();
                }
            }
        );
        this.parentObserverHandler = null;
        this.listenerHandler = new ListenerHandler(
            this,
            this.el.node,
            "addEventListener",
            "removeEventListener"
        );
    }

    init(enable = true) {
        this.enabled = enable;
        if (enable) {
            this.semParent = this.getSemParent();
            this.prevParentWidth = this.semParent.width();
            this.prevParentHeight = this.semParent.height();
            this.prevParentPos = new Vector2D(
                this.semParent.x(),
                this.semParent.y()
            );
            this.calcElToParentOffsets();
            this.addObserverToParent();
            this.elObserverHandler.addObserver();

            this.listenerHandler.on(SvgElementParentChanged.name, evt => {
                if (evt.instance !== this.el) {
                    return;
                }
                this.onParentChange(evt.newParent);
            });
        }
        else {
            this.listenerHandler.removeAllListeners();
            if (this.parentObserverHandler) {
                this.parentObserverHandler.removeObserver();
            }
            this.elObserverHandler.removeObserver();
        }
    }

    getSemParent() {
        const parent = this.el.parent();

        if (parent instanceof Svg && parent.isRoot()) {
            return parent;
        }

        const parentOfParent = parent.parent();
        const parentId = parent.data("children-from-node-id");

        return Array.from(
            parentOfParent.children()
        )
            .find(
                child => child.data("node-id") === parentId
            );
    }

    addObserverToParent() {
        this.parentObserverHandler = new ObserverHandler(
            this.semParent,
            () => {
                const currParentWidth = this.semParent.width();
                const currParentHeight = this.semParent.height();
                const currParentPos = new Vector2D(
                    this.semParent.x(),
                    this.semParent.y()
                );

                // if pos of parent changes update child pos
                if ((currParentPos.x !== this.prevParentPos.x
                || currParentPos.y !== this.prevParentPos.y)
                && currParentWidth === this.prevParentWidth
                && currParentHeight === this.prevParentHeight) {
                    this.el.x(currParentPos.x + this.xOffsetToParent);
                    this.el.y(currParentPos.y + this.yOffsetToParent);
                }

                // update prev pos, width and height on change
                // update offsets on scale change
                if (currParentWidth !== this.prevParentWidth) {
                    this.calcElToParentOffsets();
                    this.prevParentWidth = currParentWidth;
                }
                if (currParentHeight !== this.prevParentHeight) {
                    this.calcElToParentOffsets();
                    this.prevParentHeight = currParentHeight;
                }
                if (currParentPos.x !== this.prevParentPos.x
                    || currParentPos.y !== this.prevParentPos.y) {
                    this.prevParentPos = currParentPos;
                }
            }
        );
        this.parentObserverHandler.addObserver();
    }

    onParentChange(newParent) {
        // remove listeners from old parent
        if (this.parentObserverHandler) {
            this.parentObserverHandler.removeObserver();
        }
        this.semParent = newParent;
        this.prevParentWidth = this.semParent.width();
        this.prevParentHeight = this.semParent.height();
        this.prevParentPos = new Vector2D(
            this.semParent.x(),
            this.semParent.y()
        );
        this.calcElToParentOffsets();
        this.addObserverToParent();
    }

    calcElToParentOffsets() {
        this.xOffsetToParent = this.el.x() - this.semParent.x();
        this.yOffsetToParent = this.el.y() - this.semParent.y();
    }
}

extend(Element, {
    moveWithParentOnDrag(
        enable = true
    ) {
        const handler = this.remember("_move_with_parent_on_drag")
            || new MoveWithParentOnDragHandler(this);
        handler.init(enable);
        return this;
    }
});
