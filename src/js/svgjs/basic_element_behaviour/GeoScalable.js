import {
    Element,
    extend,
    Svg
} from "@svgdotjs/svg.js";
import {
    SvgElementParentChanged
} from "../../events/SvgElementEvents";
import ObserverHandler from "../../helper/ObserverHandler";
import ListenerHandler from "../../helper/ListenerHandler";

class GeoScalableHandler {
    constructor(el) {
        el.remember("_geo_scalable", this);
        this.enabled = false;
        this.el = el;
        this.semParent = null;
        this.prevParentWidth = null;
        this.prevParentHeight = null;
        this.partXOfParentWidth = null;
        this.partYOfParentHeight = null;
        this.elObserverHandler = new ObserverHandler(
            this.el,
            () => {
                if (this.semParent
                    && this.semParent.width() === this.prevParentWidth
                    && this.semParent.height() === this.prevParentHeight) {
                    this.calcPartsOfParentWidthAndHeight();
                }
            }
        );
        this.parentObserverHandler = null;
        this.listenerHandler = new ListenerHandler(
            this,
            this.el.node,
            "addEventListener",
            "removeEventListener"
        );
    }

    init(enable = true) {
        this.enabled = enable;
        if (enable) {
            this.semParent = this.getSemParent();
            this.prevParentWidth = this.semParent.width();
            this.prevParentHeight = this.semParent.height();
            this.calcPartsOfParentWidthAndHeight();
            this.addObserverToParent();
            this.elObserverHandler.addObserver();

            this.listenerHandler.on(SvgElementParentChanged.name, evt => {
                if (evt.instance !== this.el) {
                    return;
                }
                this.onParentChange(evt.newParent);
            });
        }
        else {
            this.listenerHandler.removeAllListeners();
            if (this.parentObserverHandler) {
                this.parentObserverHandler.removeObserver();
            }
            this.elObserverHandler.removeObserver();
        }
    }

    getSemParent() {
        const parent = this.el.parent();

        if (parent instanceof Svg && parent.isRoot()) {
            return parent;
        }

        const parentOfParent = parent.parent();
        const parentId = parent.data("children-from-node-id");

        return Array.from(
            parentOfParent.children()
        )
            .find(
                child => child.data("node-id") === parentId
            );
    }

    addObserverToParent() {
        this.parentObserverHandler = new ObserverHandler(
            this.semParent,
            () => {
                this.scale();
            }
        );
        this.parentObserverHandler.addObserver();
    }

    scale() {
        const currParentWidth = this.semParent.width();
        const currParentHeight = this.semParent.height();

        if (currParentWidth === this.prevParentWidth
            && currParentHeight === this.prevParentHeight) {
            return;
        }

        const parentWidthDiff = currParentWidth - this.prevParentWidth;
        const parentHeightDiff = currParentHeight - this.prevParentHeight;

        const widthFactor = parentWidthDiff / this.prevParentWidth;
        const heightFactor = parentHeightDiff / this.prevParentHeight;

        const newWidth = this.el.width()
            + widthFactor * this.el.width();
        const newHeight = this.el.height()
            + heightFactor * this.el.height();

        this.el.width(newWidth);
        this.el.height(newHeight);

        const newXPos = this.semParent.x() + this.partXOfParentWidth
            * currParentWidth;
        const newYPos = this.semParent.y() + this.partYOfParentHeight
            * currParentHeight;

        this.el.x(newXPos);
        this.el.y(newYPos);

        this.prevParentWidth = currParentWidth;
        this.prevParentHeight = currParentHeight;
    }

    onParentChange(newParent) {
        // remove listeners from old parent
        if (this.parentObserverHandler) {
            this.parentObserverHandler.removeObserver();
        }
        this.semParent = newParent;
        this.prevParentWidth = this.semParent.width();
        this.prevParentHeight = this.semParent.height();
        this.calcPartsOfParentWidthAndHeight();
        this.addObserverToParent();
    }

    calcPartsOfParentWidthAndHeight() {
        const diffXPos = this.el.x() - this.semParent.x();
        const diffYPos = this.el.y() - this.semParent.y();
        this.partXOfParentWidth = diffXPos / this.prevParentWidth;
        this.partYOfParentHeight = diffYPos / this.prevParentHeight;
    }
}

extend(Element, {
    geoScalable(
        enable = true
    ) {
        const handler = this.remember("_geo_scalable")
            || new GeoScalableHandler(this);
        handler.init(enable);
        return this;
    },

    isGeoScalable() {
        const handler = this.remember("_geo_scalable");
        return handler !== undefined && handler.enabled;
    },
});
