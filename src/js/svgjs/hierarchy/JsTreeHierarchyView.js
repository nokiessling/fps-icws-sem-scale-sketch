import $ from "jquery";
import "jstree";
import {
    HierarchyNodeDeleted,
    HierarchyNodeMoved,
    HierarchyNodeRenamed,
    HierarchyNodeSelected
} from "../../events/HierarchyNodeEvents";

export default class JsTreeHierarchyView {
    constructor(htmlHierarchyId) {
        this.htmlHierarchyId = htmlHierarchyId;
        this.htmlHierarchyRoot = null;

        this.initHierarchy();
    }

    initHierarchy() {
        this.htmlHierarchyRoot = $(this.htmlHierarchyId);
        const existingJsTreeInstance = $.jstree
            .reference(this.htmlHierarchyRoot);
        if (existingJsTreeInstance) {
            existingJsTreeInstance.destroy(false);
        }
        this.htmlHierarchyRoot.jstree(
            {
                core: {
                    data: [
                        {
                            text: "Sketch",
                            id: 0,
                            state: {
                                opened: true,
                                selected: true
                            },
                            children: []
                        }
                    ],
                    check_callback: true,
                },
                dnd: {
                    copy: false,
                    // use_html5: true,
                },
                plugins: [
                    "dnd", "wholerow",
                ]
            }
        );

        this.registerHierarchyEvents();
    }

    registerHierarchyEvents() {
        this.htmlHierarchyRoot.on("loaded.jstree", evt => {
            this.htmlHierarchyRoot.jstree("open_all");
        });

        this.htmlHierarchyRoot.on("refresh.jstree", evt => {
            this.htmlHierarchyRoot.jstree("open_all");
        });

        this.htmlHierarchyRoot.on("select_node.jstree", (evt, data) => {
            document.dispatchEvent(
                new HierarchyNodeSelected(data.node.id)
            );
        });

        this.htmlHierarchyRoot.on("move_node.jstree", (evt, data) => {
            document.dispatchEvent(
                new HierarchyNodeMoved(data.node.id, data.node.parent)
            );
        });

        // enable rename
        this.htmlHierarchyRoot.on("dblclick.jstree", evt => {
            const jstree = this.htmlHierarchyRoot.jstree(true);
            const selectedNode = this.htmlHierarchyRoot.jstree(
                "get_selected", true
            );
            if (selectedNode && selectedNode.length > 0
                && selectedNode[0].id !== "0") {
                jstree.edit(selectedNode[0]);
            }
        });

        this.htmlHierarchyRoot.on("rename_node.jstree", (evt, data) => {
            document.dispatchEvent(
                new HierarchyNodeRenamed(data.node.id, data.text)
            );
        });

        this.htmlHierarchyRoot.on("delete_node.jstree", (evt, data) => {
            document.dispatchEvent(
                new HierarchyNodeDeleted(data.node.id)
            );
        });
    }

    rebuildHierarchy(tree) {
        const jsTree = this.htmlHierarchyRoot.jstree(true);
        jsTree.settings.core.data = [];
        jsTree.settings.core.data.push(
            this.generateJsTreeEleForNode(tree.root)
        );
        jsTree.refresh();
    }

    generateJsTreeEleForNode(node) {
        const jsTreeEle = {
            text: node.name,
            id: node.id.toString(),
            state: {
                opened: true,
                selected: false
            },
            children: []
        };
        for (let i = 0; i < node.children.length; i++) {
            jsTreeEle.children.push(
                this.generateJsTreeEleForNode(node.children[i])
            );
        }
        return jsTreeEle;
    }

    deselectAllNodes() {
        this.htmlHierarchyRoot.jstree(true).deselect_all();
    }

    selectNode(id) {
        const selectedNodes = this.htmlHierarchyRoot.jstree().get_selected();
        if (selectedNodes && selectedNodes.length > 0) {
            if (selectedNodes.includes(id.toString())) {
                return;
            }
            this.deselectAllNodes();
        }
        this.htmlHierarchyRoot.jstree(true).select_node(id.toString());
    }

    deleteNode(id) {
        const node = this.htmlHierarchyRoot.jstree().get_node(id.toString());
        if (node) {
            this.htmlHierarchyRoot.jstree(true)
                .delete_node(id.toString());
        }
    }
}
