import { Container } from "@svgdotjs/svg.js";

import {
    SvgElementCreated,
    SvgElementDeleted,
    SvgElementMoved,
    SvgElementParentChanged,
    SvgElementSelected,
    SvgElementSelectedInHierarchy
} from "../../events/SvgElementEvents";
import NodeTree from "../../tree/NodeTree";
import JsTreeHierarchyView from "./JsTreeHierarchyView";
import {
    HierarchyNodeDeleted,
    HierarchyNodeMoved,
    HierarchyNodeRenamed,
    HierarchyNodeSelected
} from "../../events/HierarchyNodeEvents";
import ListenerHandler from "../../helper/ListenerHandler";

export default class SvgHierarchyManager {
    static Instance;

    constructor(svg) {
        this.svg = svg;
        if (SvgHierarchyManager.Instance) {
            SvgHierarchyManager.Instance.removeListeners();
        }
        SvgHierarchyManager.Instance = this;
        this.elementTree = new NodeTree(this.svg);
        this.view = new JsTreeHierarchyView("#hierarchy");

        this.htmlNameNodeId = "node-id";
        this.htmlNameGroupParentId = "children-from-node-id";
        this.htmlNameIsSketchPart = "sketchPart";

        this.listenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );

        this.addListeners();
    }

    reset(svg) {
        this.svg = svg;
        this.elementTree = new NodeTree(this.svg);
        this.view = new JsTreeHierarchyView("#hierarchy");
    }

    addListeners() {
        this.listenerHandler.on(SvgElementCreated.name, evt => {
            this.onSvgElementCreated(evt.instance);
        });
        this.listenerHandler.on(SvgElementMoved.name, evt => {
            this.onSvgElementMoved(evt.instance);
        });
        this.listenerHandler.on(SvgElementSelected.name, evt => {
            this.onSvgElementSelected(evt.instance);
        });
        this.listenerHandler.on(SvgElementDeleted.name, evt => {
            this.onSvgElementDeleted(evt.instance);
        });

        this.listenerHandler.on(HierarchyNodeSelected.name, evt => {
            this.onHierarchyNodeSelected(evt.id);
        });
        this.listenerHandler.on(HierarchyNodeMoved.name, evt => {
            this.onHierarchyNodeMoved(evt.id, evt.parentId);
        });
        this.listenerHandler.on(HierarchyNodeRenamed.name, evt => {
            this.onHierarchyNodeRenamed(evt.id, evt.newName);
        });
        this.listenerHandler.on(HierarchyNodeDeleted.name, evt => {
            this.onHierarchyNodeDeleted(evt.id);
        });
    }

    removeListeners() {
        this.listenerHandler.removeAllListeners();
    }

    onSvgElementCreated(instance) {
        instance.data(this.htmlNameIsSketchPart, true);

        const parent = this.findParent(
            this.svg,
            instance.bbox().x,
            instance.bbox().y,
            instance.bbox().width,
            instance.bbox().height
        );

        if (parent && parent !== this.svg) {
            const group = this.getOrCreateChildrensGroup(parent);
            group.add(instance);
        }

        const elementId = this.elementTree.addElement(instance, parent);

        instance.data(this.htmlNameNodeId, elementId);
        this.view.rebuildHierarchy(this.elementTree);

        this.view.selectNode(elementId);
    }

    onSvgElementMoved(instance) {
        const oldParent = this.getParent(instance);
        const newParent = this.findParent(
            this.svg,
            instance.bbox().x,
            instance.bbox().y,
            instance.bbox().width,
            instance.bbox().height
        );

        if (oldParent === newParent) return;

        this.regroupSvgElement(instance, oldParent, newParent);
        this.elementTree.moveElement(instance, newParent);
        this.view.rebuildHierarchy(this.elementTree);

        instance.node.dispatchEvent(
            new SvgElementParentChanged(instance, newParent)
        );
    }

    onSvgElementSelected(instance) {
        const id = this.elementTree.getId(instance);
        if (id) {
            this.view.selectNode(id);
        }
    }

    onSvgElementDeleted(instance) {
        const id = this.elementTree.getId(instance);
        if (!id) return;

        this.elementTree.removeElement(instance);
        this.view.rebuildHierarchy(this.elementTree);
        // this.view.deleteNode(id);
    }

    findParent(root, x, y, width, height) {
        const isSmaller = (first, second) => {
            if (!first) {
                return true;
            }

            const firstBB = first.bbox();
            const secondBB = second.bbox();

            return (firstBB.width * firstBB.height)
                >= (secondBB.width * secondBB.height);
        };

        let semParent = root;
        const children = root.children();
        for (let i = 0; i < children.length; i++) {
            const currChild = children[i];
            if (!currChild.data(this.htmlNameIsSketchPart)
            || (currChild.node.classList !== undefined
                && (currChild.node.classList.contains("selection-highlighting")
                    || currChild.node.classList.contains("selectable-clone")))) {
                console.log("child ", currChild, "is no sketch part, skipping");
                continue;
            }
            if (currChild.inside(x, y)
                && currChild.inside(x + width, y + height)) {
                if (currChild instanceof Container) {
                    const nestedParent = this
                        .findParent(currChild, x, y, width, height);
                    if (isSmaller(semParent, nestedParent)) {
                        semParent = nestedParent;
                    }
                }
                else if (isSmaller(semParent, currChild)) {
                    semParent = currChild;
                }
            }
        }

        if (semParent !== this.svg && semParent instanceof Container) {
            const groupId = semParent.data(this.htmlNameGroupParentId);
            semParent = Array.from(semParent.parent().children()).find(
                child => child.data(this.htmlNameNodeId) === groupId
            );
        }

        return semParent;
    }

    getParent(element) {
        return this.elementTree.getParentElement(element);
    }

    getOrCreateChildrensGroup(parent) {
        const parentId = parent.data(this.htmlNameNodeId);

        const candidate = parent.parent();

        let group = Array.from(candidate.children()).find(
            child => child.data(this.htmlNameGroupParentId) === parentId
        );

        if (!group) {
            group = candidate.group();
            group.data(this.htmlNameGroupParentId, parentId);
            group.data(this.htmlNameIsSketchPart, true);
        }

        return group;
    }

    onHierarchyNodeSelected(id) {
        const element = this.elementTree.getElementById(id);
        element.node.dispatchEvent(
            new SvgElementSelectedInHierarchy(element)
        );
    }

    onHierarchyNodeMoved(id, parentId) {
        const element = this.elementTree.getElementById(id);
        const oldParent = this.getParent(element);
        const newParent = this.elementTree.getElementById(parentId);

        this.regroupSvgElement(element, oldParent, newParent);
        this.elementTree.moveElement(element, newParent);

        element.node.dispatchEvent(
            new SvgElementParentChanged(element, newParent)
        );
    }

    onHierarchyNodeRenamed(id, name) {
        const element = this.elementTree.getElementById(id);
        this.elementTree.renameNode(element, name);
    }

    onHierarchyNodeDeleted(id) {
        const element = this.elementTree.getElementById(id);
        if (!element) return;
        this.elementTree.removeElement(element);
        element.remove();
    }

    regroupSvgElement(element, oldParent, newParent) {
        if (!newParent) {
            return;
        }

        if (newParent === oldParent) {
            return;
        }

        if (newParent !== this.svg) {
            const oldGroup = element.parent();
            const newGroup = this.getOrCreateChildrensGroup(newParent);

            element.toParent(newGroup);
            if (oldGroup.children().length === 0) {
                oldGroup.remove();
            }
        }
        else {
            element.toParent(newParent);
        }
    }

    getElementName(element) {
        return this.elementTree.getName(element);
    }

    getElementId(element) {
        return this.elementTree.getId(element);
    }
}
