import { Element, extend } from "@svgdotjs/svg.js";
import ObserverHandler from "../../helper/ObserverHandler";
import ListenerHandler from "../../helper/ListenerHandler";
import { SvgElementMoved } from "../../events/SvgElementEvents";
import { selectionColor } from "../../utils/Colors";

class EasierSelectableHandler {
    static hoverColor = selectionColor;

    constructor(el, selectableWidth, onMousedown, isDraggable) {
        el.remember("_easier_selectable", this);
        this.el = el;
        this.selectableWidth = selectableWidth;
        this.onMousedown = onMousedown;
        this.isDraggable = isDraggable;
        this.selectableClone = null;
        this.listenerHandler = null;
        this.elObserverHandler = new ObserverHandler(
            this.el,
            () => this.updateSelectableClone()
        );
        this.cloneObserverHandler = null;
    }

    init(enable = true) {
        if (enable) {
            this.createSelectableClone();
            this.addListener();
            this.elObserverHandler.addObserver();
            if (this.isDraggable) {
                this.activateDraggable();
            }
        }
        else {
            if (this.isDraggable) {
                this.deactivateDraggable();
            }
            this.removeSelectableClone();
            this.removeListener();
            this.elObserverHandler.removeObserver();
        }
    }

    createSelectableClone() {
        if (this.selectableClone) {
            this.removeSelectableClone();
        }

        this.selectableClone = this.el
            .clone()
            .addTo(this.el.parent())
            .stroke({
                color: EasierSelectableHandler.hoverColor,
                opacity: 0,
                width: this.selectableWidth,
            })
            .front();
        this.selectableClone.node.classList.add("selectable-clone");
    }

    updateSelectableClone() {
        if (!this.selectableClone) return;

        if (this.el.x() !== this.selectableClone.x()
            || this.el.y() !== this.selectableClone.y()) {
            this.selectableClone.move(this.el.x(), this.el.y());
        }
        if (this.el.width() !== this.selectableClone.width()
            || this.el.height() !== this.selectableClone.height()) {
            this.selectableClone.size(this.el.width(), this.el.height());
        }
        const transformName = "transform";
        const elTransform = this.el.attr(transformName);
        if (elTransform && elTransform !== "") {
            this.selectableClone.attr(transformName, elTransform);
        }
    }

    removeSelectableClone() {
        if (!this.selectableClone) return;

        this.selectableClone.remove();
        this.selectableClone = null;
    }

    addListener() {
        if (this.listenerHandler) {
            this.removeListener();
        }

        this.listenerHandler = new ListenerHandler(
            this,
            this.selectableClone,
            "on",
            "off"
        );
        this.listenerHandler.on("mousedown", e => {
            this.onMousedown();
        }, this.selectableClone);
        this.listenerHandler.on("mouseover", e => {
            if (this.selectableClone.attr("stroke-color")
                !== EasierSelectableHandler.hoverColor) {
                this.selectableClone
                    .stroke({ color: EasierSelectableHandler.hoverColor });
            }
            this.selectableClone.stroke({ opacity: 0.3 });
        }, this.selectableClone);
        this.listenerHandler.on("mouseout", e => {
            this.selectableClone.stroke({ opacity: 0 });
        }, this.selectableClone);
    }

    removeListener() {
        if (!this.listenerHandler) {
            return;
        }

        this.listenerHandler.removeAllListeners();
        this.listenerHandler = null;
    }

    updateSelectableWidth(newSelectableWidth) {
        if (!this.selectableClone) {
            return;
        }

        this.selectableClone.stroke({ width: newSelectableWidth });
        this.selectableWidth = newSelectableWidth;
    }

    toggleDraggable(shouldBeDraggable) {
        if (shouldBeDraggable) {
            this.activateDraggable();
        }
        else {
            this.deactivateDraggable();
        }
        this.isDraggable = shouldBeDraggable;
    }

    activateDraggable() {
        this.selectableClone.draggable()
            .on("dragend", e => {
                this.el.node.dispatchEvent(
                    new SvgElementMoved(this.el)
                );
            });
        this.cloneObserverHandler = new ObserverHandler(
            this.selectableClone,
            () => {
                if (this.el.x() !== this.selectableClone.x()
                    || this.el.y() !== this.selectableClone.y()) {
                    this.el.move(
                        this.selectableClone.x(),
                        this.selectableClone.y()
                    );
                }
                // this.el.center(
                //     this.selectableClone.cx(),
                //     this.selectableClone.cy()
                // );
            }
        );
        this.cloneObserverHandler.addObserver();
    }

    deactivateDraggable() {
        if (this.cloneObserverHandler === null) {
            return;
        }

        this.selectableClone.draggable(false);
        this.cloneObserverHandler.removeObserver();
        this.cloneObserverHandler = null;
    }
}

extend(Element, {
    easierSelectable(
        enable = true,
        selectableWidth = 10,
        onMousedown = () => {},
        isDraggable = false,
    ) {
        let handler = this.remember("_easier_selectable");
        if (handler) {
            handler.init(enable);
            this.updateEasierSelectable(
                selectableWidth,
                onMousedown,
                isDraggable
            );
        }
        else {
            handler = new EasierSelectableHandler(
                this,
                selectableWidth,
                onMousedown,
                isDraggable,
            );
            handler.init(enable);
        }
        return this;
    },

    updateEasierSelectable(
        selectableWidth = undefined,
        onMousedown = undefined,
        isDraggable = undefined,
    ) {
        const handler = this.remember("_easier_selectable");

        if (!handler) {
            return;
        }

        if (selectableWidth !== undefined) {
            handler.updateSelectableWidth(selectableWidth);
        }

        if (onMousedown !== undefined) {
            handler.onMousedown = onMousedown;
        }

        if (isDraggable !== undefined) {
            handler.toggleDraggable(isDraggable);
        }
    },

    changeEasierSelectableColor(
        hoverColor = undefined
    ) {
        if (hoverColor !== undefined) {
            EasierSelectableHandler.hoverColor = hoverColor;
        }
    }
});
