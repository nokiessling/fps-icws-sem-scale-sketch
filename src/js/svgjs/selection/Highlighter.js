import { Element, extend } from "@svgdotjs/svg.js";
import ObserverHandler from "../../helper/ObserverHandler";

class HighlightHandler {
    constructor(el, highlightColor, highlightOpacity, highlightWidth) {
        el.remember("_highlight", this);
        this.el = el;
        this.highlightColor = highlightColor;
        this.highlightOpacity = highlightOpacity;
        this.highlightWidth = highlightWidth;
        this.highlightClone = null;
        this.observerHandler = new ObserverHandler(
            this.el,
            () => this.updateHighlighting()
        );
    }

    init(enable = true) {
        if (enable) {
            this.highlight();
            this.observerHandler.addObserver();
        }
        else {
            this.removeHighlighting();
            this.observerHandler.removeObserver();
        }
    }

    highlight() {
        if (this.highlightClone) {
            this.removeHighlighting();
        }
        // cloning element using use and changing the style
        // (in css) doesn't seem to work
        // setting stroke here doesn't work as stroke is already defined on el
        // and thus is ignored on use element(s)
        // this.highlightClone = this.el.root()
        //     .use(this.el);
        // this.highlightClone
        //     .node
        //     .classList.add("highlighted");

        this.highlightClone = this.el
            .clone()
            .addTo(this.el.parent())
            .stroke({
                color: this.highlightColor,
                opacity: this.highlightOpacity,
                width: this.highlightWidth,
            })
            .back();
        this.highlightClone.node.classList.add("selection-highlighting");
    }

    updateHighlighting() {
        if (!this.highlightClone) return;

        this.highlightClone.move(this.el.x(), this.el.y());
        this.highlightClone.size(this.el.width(), this.el.height());
        const transformName = "transform";
        const elTransform = this.el.attr(transformName);
        if (elTransform && elTransform !== "") {
            this.highlightClone.attr(transformName, elTransform);
        }
    }

    removeHighlighting() {
        if (!this.highlightClone) return;

        this.highlightClone.remove();
        this.highlightClone = null;
    }
}

extend(Element, {
    highlight(
        enable = true,
        highlightColor = "#87ceeb", // "#88a174",
        highlightOpacity = 0.5,
        highlightWidth = 10
    ) {
        const handler = this.remember("_highlight")
            || new HighlightHandler(
                this,
                highlightColor,
                highlightOpacity,
                highlightWidth
            );
        handler.init(enable);
        return this;
    }
});
