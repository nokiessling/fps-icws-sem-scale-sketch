import { SVG, Svg } from "@svgdotjs/svg.js";
import {
    SvgElementCreated,
    SvgElementDeleted,
    SvgElementSelected,
    SvgElementDeselected,
    SvgElementSelectedInHierarchy
} from "../../events/SvgElementEvents";
import ListenerHandler from "../../helper/ListenerHandler";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";
import { selectionColor } from "../../utils/Colors";

export default class SvgSelectionManager {
    static Instance = new SvgSelectionManager();

    constructor() {
        if (SvgSelectionManager.Instance) {
            SvgSelectionManager.Instance.removeAllListeners();
        }
        SvgSelectionManager.Instance = this;

        this.selectedElement = null;
        this.isInteractableSelection = false;
        this.isDraggableSelection = false;
        this.isScalableSelection = false;

        this.basicListenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );
        this.interactableSelectionListenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );

        this.addBasicListeners();
    }

    reset() {
        if (this.isInteractableSelection) {
            this.removeInteractableListeners();
        }

        this.selectedElement = null;
        this.isInteractableSelection = false;
        this.isDraggableSelection = false;
        this.isScalableSelection = false;
    }

    addBasicListeners() {
        this.basicListenerHandler.on(SvgElementCreated.name, evt => {
            this.onElementCreated(evt.instance);
        });
        this.basicListenerHandler.on(
            SvgElementSelectedInHierarchy.name,
            evt => {
                this.selectElement(evt.instance);
            }
        );
        this.basicListenerHandler.on(SvgElementDeleted.name, evt => {
            this.onElementDeleted(evt.instance);
        });
    }

    removeAllListeners() {
        this.basicListenerHandler.removeAllListeners();

        if (this.isInteractableSelection) {
            this.removeInteractableListeners();
        }
    }

    removeInteractableListeners() {
        if (!this.isInteractableSelection) {
            return;
        }

        this.interactableSelectionListenerHandler.removeAllListeners();
        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );
        existingElements.forEach(element => {
            element.easierSelectable(false);
        });
    }

    activateInteractableSelection(
        existingElements,
        draggable = true,
        scalable = false,
        hoverColor = selectionColor
    ) {
        SVG(document.querySelector("svg"))
            .changeEasierSelectableColor(hoverColor);

        existingElements.forEach(element => {
            if (!(element instanceof Svg)) { // && !parent.isRoot()) {
                element.easierSelectable(
                    true,
                    10,
                    () => {
                        this.selectElement(element);
                    },
                    draggable
                );
            }
        });

        this.interactableSelectionListenerHandler.on(
            SvgElementCreated.name, evt => {
                evt.instance.easierSelectable(
                    true,
                    10,
                    () => {
                        this.selectElement(evt.instance);
                    },
                    draggable
                );
            }
        );

        if (this.selectedElement && scalable) {
            this.enableScalableBehaviours(this.selectedElement);
        }

        this.isInteractableSelection = true;
        this.isDraggableSelection = draggable;
        this.isScalableSelection = scalable;
    }

    deactivateInteractableSelection(existingElements) {
        this.interactableSelectionListenerHandler.removeAllListeners();
        if (this.isInteractableSelection) {
            existingElements.forEach(element => {
                element.easierSelectable(false);
            });

            SVG(document.querySelector("svg"))
                .changeEasierSelectableColor(selectionColor);
        }

        if (this.selectedElement && this.isScalableSelection) {
            this.disableScalableBehaviours(this.selectedElement);
        }

        this.isInteractableSelection = false;
        this.isDraggableSelection = false;
        this.isScalableSelection = false;
    }

    onElementCreated(element) {
        this.selectElement(element);
    }

    onElementDeleted(element) {
        this.deselectElement(element);
        if (this.isInteractableSelection) {
            this.interactableSelectionListenerHandler.removeTarget(element);
            element.easierSelectable(false);
        }
    }

    selectElement(element) {
        if (this.selectedElement === element) {
            return;
        }

        if (this.selectedElement) {
            this.deselectElement(this.selectedElement);
        }

        this.selectedElement = element;
        if (this.isScalableSelection) {
            this.enableScalableBehaviours(this.selectedElement);
        }
        this.selectedElement.highlight();
        this.selectedElement.node.dispatchEvent(
            new SvgElementSelected(this.selectedElement)
        );
    }

    deselectElement(element) {
        if (element === null) {
            return;
        }

        if (this.isScalableSelection) {
            this.disableScalableBehaviours(element);
        }
        element.highlight(false);
        element.node.dispatchEvent(
            new SvgElementDeselected(element)
        );
        if (this.selectedElement === element) {
            this.selectedElement = null;
        }
    }

    enableScalableBehaviours(element) {
        element.selectize(true, { deepSelect: true });
        element.resize();
    }

    disableScalableBehaviours(element) {
        element.selectize(false, { deepSelect: false });
        element.resize(false);
    }
}
