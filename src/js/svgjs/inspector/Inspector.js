import {
    SvgElementDeleted,
    SvgElementMetadataChanged,
    SvgElementSelected
} from "../../events/SvgElementEvents";
import SemRepeater from "../semantic_scale/SemRepeater";
import SemStretcher from "../semantic_scale/SemStretcher";
import SemAnchorer from "../semantic_scale/SemAnchorer";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";

export default class Inspector {
    constructor(
        htmlId = "#inspector",
        htmlEleNameId = "#inspector-name",
        htmlEleTypeId = "#inspector-type",
        htmlComponentsId = "#inspector-components",
        htmlSemanticRepeatCardId = "#inspector-semantic-repeat-card",
        htmlSemanticStretchCardId = "#inspector-semantic-stretch-card",
        htmlSemanticAnchorCardId = "#inspector-semantic-anchor-card"
    ) {
        this.htmlInspector = document.querySelector(htmlId);
        this.htmlEleName = document.querySelector(htmlEleNameId);
        this.htmlEleType = document.querySelector(htmlEleTypeId);
        this.htmlComponents = document.querySelector(htmlComponentsId);
        this.htmlSemanticRepeatCard = document
            .querySelector(htmlSemanticRepeatCardId);
        this.htmlSemanticStretchCard = document
            .querySelector(htmlSemanticStretchCardId);
        this.htmlSemanticAnchorCard = document
            .querySelector(htmlSemanticAnchorCardId);
        this.currentEle = null;
        this.repeatable = false;

        this.htmlInspector.hidden = true;

        document.addEventListener(SvgElementSelected.name, evt => {
            this.showEleMetadata(evt.instance);
        });

        document.addEventListener(SvgElementMetadataChanged.name, evt => {
            this.showEleMetadata(evt.instance);
        });

        document.addEventListener(SvgElementDeleted.name, evt => {
            if (evt.instance === this.currentEle) {
                this.htmlInspector.hidden = true;
            }
        });

        const removeRepeatBehaviourButton = this.htmlSemanticRepeatCard
            .querySelector("#remove-sem-repeat-btn");
        removeRepeatBehaviourButton.addEventListener("click", evt => {
            SemRepeater.removeRepeatableBehaviour(this.currentEle);
            if (!SemStretcher.isSemStretchable(this.currentEle)
                && !SemAnchorer.isSemAnchorable(this.currentEle)) {
                this.currentEle.geoScalable();
            }
        });

        const repeatTypeBtns = this.htmlSemanticRepeatCard
            .querySelector("#inspector-repeat-types-btn-group")
            .querySelectorAll("button");
        repeatTypeBtns.forEach(btn => btn.addEventListener("click", evt => {
            this.onSemRepeatTypeButtonClick(btn);
        }));

        const directionsDiv = this.htmlSemanticRepeatCard
            .querySelector("div[data-repeat-type=\"directions\"]");
        const dirInputs = directionsDiv
            .querySelectorAll(".repeat-direction");
        dirInputs.forEach(input => input.addEventListener("change", evt => {
            const repeatVec = SemRepeater.vectorMap
                .get(input.dataset.repeatDirection);
            const newAmount = Number.parseInt(input.value, 10);
            SemRepeater.updateAmountOfDir(
                this.currentEle, repeatVec, newAmount, true
            );
        }));

        const gridDiv = this.htmlSemanticRepeatCard
            .querySelector("div[data-repeat-type=\"grid\"]");
        const symmetricRepeatCheckbox = gridDiv
            .querySelector("#repeat-symmetrically");
        const symmetricAmountDivs = gridDiv.querySelectorAll(
            ".repeat-amount-symmetric"
        );
        const asymmetricAmountDivs = gridDiv.querySelectorAll(
            ".repeat-amount-asymmetric"
        );
        symmetricRepeatCheckbox.addEventListener("click", evt => {
            if (symmetricRepeatCheckbox.checked) {
                symmetricAmountDivs.forEach(div => {
                    div.classList.remove("d-none");
                });
                asymmetricAmountDivs.forEach(div => {
                    div.classList.add("d-none");
                });
                SemRepeater.updateRepeatSymmetrically(this.currentEle, true);
            }
            else {
                asymmetricAmountDivs.forEach(div => {
                    div.classList.remove("d-none");
                });
                symmetricAmountDivs.forEach(div => {
                    div.classList.add("d-none");
                });
                SemRepeater.updateRepeatSymmetrically(this.currentEle, false);
            }
        });
        const gridAmountInputs = gridDiv
            .querySelectorAll(".repeat-grid-amount");
        gridAmountInputs.forEach(input => {
            input.addEventListener("change", evt => {
                const repeatVec = SemRepeater.vectorMap
                    .get(input.dataset.repeatDirection);
                const newAmount = Number.parseInt(input.value, 10);
                SemRepeater.updateAmountOfDir(
                    this.currentEle,
                    repeatVec,
                    newAmount
                );
            });
        });

        const customDirsDiv = this.htmlSemanticRepeatCard
            .querySelector("div[data-repeat-type=\"direction\"]");
        const addCustomDirBtn = customDirsDiv
            .querySelector("#repeat-add-custom-dir-btn");
        addCustomDirBtn.addEventListener("click", evt => {
            SemRepeater.activateAddingOfCustomDirection(this.currentEle);
        });

        const pathDiv = this.htmlSemanticRepeatCard
            .querySelector("div[data-repeat-type=\"path\"]");
        const definePathButton = pathDiv
            .querySelector("#repeat-define-path-btn");
        definePathButton.addEventListener("click", evt => {
            SemRepeater.activateAddingOfPath(this.currentEle);
        });
        const pathShowBtn = pathDiv
            .querySelector("#repeat-path-show-btn");
        pathShowBtn.addEventListener("click", evt => {
            const { path } = SemRepeater
                .getSemRepeatableConfigOfElement(this.currentEle);
            if (path.node.classList.contains("d-none")) {
                path.node.classList.remove("d-none");
            }
            else {
                path.node.classList.add("d-none");
            }
        });
        const pathEditBtn = pathDiv
            .querySelector("#repeat-path-edit-btn");
        pathEditBtn.addEventListener("click", evt => {
            // TODO: enable editing of path
        });
        const pathDeleteBtn = pathDiv
            .querySelector("#repeat-path-delete-btn");
        pathDeleteBtn.addEventListener("click", evt => {
            SemRepeater.deletePath(this.currentEle);
        });
        const rotateAlongPathCheckbox = pathDiv
            .querySelector("#repeat-along-path-with-rotation");
        rotateAlongPathCheckbox.addEventListener("click", evt => {
            SemRepeater.updateRotateAlongPath(
                this.currentEle,
                rotateAlongPathCheckbox.checked
            );
        });

        const marginInput = this.htmlSemanticRepeatCard
            .querySelector(".repeat-margin");
        marginInput.addEventListener("input", evt => {
            SemRepeater.updateMargin(
                this.currentEle,
                Number.parseInt(marginInput.value, 10)
            );
        });

        const testIntersectionsCheckbox = this.htmlSemanticRepeatCard
            .querySelector("#repeat-test-intersections");
        testIntersectionsCheckbox.addEventListener("click", evt => {
            SemRepeater.updateTestIntersections(
                this.currentEle,
                testIntersectionsCheckbox.checked
            );
        });

        const removeStretchBehaviourButton = this.htmlSemanticStretchCard
            .querySelector("#remove-sem-stretch-btn");
        removeStretchBehaviourButton.addEventListener("click", evt => {
            SemStretcher.removeStretchableBehaviour(this.currentEle);
            if (!SemRepeater.isSemRepeatable(this.currentEle)
                && !SemAnchorer.isSemAnchorable(this.currentEle)) {
                this.currentEle.geoScalable();
            }
        });

        const stretchHorizontalFactorInput = this.htmlSemanticStretchCard
            .querySelector("#stretch-factor-horizontal");
        stretchHorizontalFactorInput.addEventListener("change", evt => {
            SemStretcher.updateHorizontalFactor(
                this.currentEle,
                Number.parseFloat(stretchHorizontalFactorInput.value)
            );
        });

        const stretchVerticalFactorInput = this.htmlSemanticStretchCard
            .querySelector("#stretch-factor-vertical");
        stretchVerticalFactorInput.addEventListener("change", evt => {
            SemStretcher.updateVerticalFactor(
                this.currentEle,
                Number.parseFloat(stretchVerticalFactorInput.value)
            );
        });

        const removeAnchorBehaviourButton = this.htmlSemanticAnchorCard
            .querySelector("#remove-sem-anchor-btn");
        removeAnchorBehaviourButton.addEventListener("click", evt => {
            SemAnchorer.removeAnchorableBehaviour(this.currentEle);
            if (SemStretcher.isSemStretchable(this.currentEle)) {
                SemStretcher.updateUpdatePos(this.currentEle, true);
            }
            if (!SemRepeater.isSemRepeatable(this.currentEle)
                && !SemStretcher.isSemStretchable(this.currentEle)) {
                this.currentEle.geoScalable();
            }
        });

        const anchorDefineBtn = this.htmlSemanticAnchorCard
            .querySelector("#anchor-define-anchor-point-btn");
        anchorDefineBtn.addEventListener("click", evt => {
            SemAnchorer.activateAddingOfAnchorPoint(this.currentEle);
        });

        const anchorEditBtn = this.htmlSemanticAnchorCard
            .querySelector("#anchor-edit-btn");
        anchorEditBtn.addEventListener("click", evt => {
            SemAnchorer.activateAddingOfAnchorPoint(this.currentEle);
        });
    }

    reset() {
        this.currentEle = null;
        this.repeatable = false;

        this.htmlInspector.hidden = true;
    }

    showEleMetadata(element) {
        this.htmlEleName.innerHTML = SvgHierarchyManager.Instance
            .getElementName(element);
        this.htmlEleType.innerHTML = element.type;

        this.repeatable = SemRepeater.isSemRepeatable(element);
        this.htmlSemanticRepeatCard.hidden = !this.repeatable;
        if (this.repeatable) {
            const {
                directions,
                repeatSymmetrically,
                path,
                rotateAlongPath,
                amount,
                margin,
                testIntersections
            } = SemRepeater.getSemRepeatableConfigOfElement(element);

            const configuredRepeatTypes = [];

            const directionsDiv = this.htmlSemanticRepeatCard
                .querySelector("div[data-repeat-type=\"directions\"]");
            const dirInputs = directionsDiv
                .querySelectorAll(".repeat-direction");
            dirInputs.forEach(input => {
                const repeatVec = SemRepeater.vectorMap.get(
                    input.dataset.repeatDirection
                );
                const index = directions.findIndex(
                    value => repeatVec.x === value.x && repeatVec.y === value.y
                );

                if (index > -1) {
                    if (!configuredRepeatTypes.includes("directions")) {
                        configuredRepeatTypes.push("directions");
                    }
                    input.value = amount
                        .get(
                            SemRepeater.getDirKeyInAmountMap(amount, repeatVec)
                        );
                }
            });

            const gridDirIndex = directions.findIndex(
                value => value.x === 0 && value.y === 0
            );
            if (gridDirIndex > -1) {
                configuredRepeatTypes.push("grid");
            }
            const gridDiv = this.htmlSemanticRepeatCard
                .querySelector("div[data-repeat-type=\"grid\"]");
            const symmetricRepeatCheckbox = gridDiv
                .querySelector("#repeat-symmetrically");
            symmetricRepeatCheckbox.checked = repeatSymmetrically;
            const symmetricAmountDivs = gridDiv.querySelectorAll(
                ".repeat-amount-symmetric"
            );
            const asymmetricAmountDivs = gridDiv.querySelectorAll(
                ".repeat-amount-asymmetric"
            );
            if (repeatSymmetrically) {
                symmetricAmountDivs.forEach(div => {
                    div.classList.remove("d-none");
                });
                asymmetricAmountDivs.forEach(div => {
                    div.classList.add("d-none");
                });
            }
            else {
                asymmetricAmountDivs.forEach(div => {
                    div.classList.remove("d-none");
                });
                symmetricAmountDivs.forEach(div => {
                    div.classList.add("d-none");
                });
            }

            const gridAmountInputs = gridDiv
                .querySelectorAll(".repeat-grid-amount");
            gridAmountInputs.forEach(input => {
                const repeatVec = SemRepeater.vectorMap.get(
                    input.dataset.repeatDirection
                );
                input.value = amount
                    .get(
                        SemRepeater.getDirKeyInAmountMap(amount, repeatVec)
                    );
            });

            const customDirsDiv = this.htmlSemanticRepeatCard
                .querySelector("div[data-repeat-type=\"direction\"]");
            if (directions.length > 0) {
                const customDirs = directions
                    .filter(value => value.x > -1 && value.x < 0
                        || value.x > 0 && value.x < 1
                        || value.y > -1 && value.y < 0
                        || value.y > 0 && value.y < 1);
                if (customDirs !== undefined && customDirs.length > 0) {
                    configuredRepeatTypes.push("direction");
                    customDirs.forEach(dir => {
                        const dirAmt = amount.get(
                            SemRepeater.getDirKeyInAmountMap(amount, dir)
                        );
                        const dirSlopeAngle = dir.slopeAngle();
                        const dirLine = customDirsDiv
                            .querySelector(
                                `[data-custom-dir-x="${dir.x}"][data-custom-dir-y="${dir.y}"]`
                            );
                        if (dirLine) {
                            dirLine.amount = dirAmt;
                        }
                        else {
                            customDirsDiv.insertAdjacentHTML(
                                "beforeend",
                                `
                                    <custom-dir-line 
                                    angle="${dirSlopeAngle}"
                                    amount="${dirAmt}" 
                                    data-custom-dir-x="${dir.x}"
                                    data-custom-dir-y="${dir.y}">
                                    </custom-dir-line>
                                `
                            );
                            const newDirLine = customDirsDiv
                                .querySelector(
                                    `[data-custom-dir-x="${dir.x}"][data-custom-dir-y="${dir.y}"]`
                                );
                            newDirLine.addEventListener("amount-changed",
                                evt => {
                                    const newAmount = Number
                                        .parseInt(newDirLine.amount, 10);
                                    SemRepeater.updateAmountOfDir(
                                        this.currentEle,
                                        dir,
                                        newAmount
                                    );
                                });
                            newDirLine.addEventListener("delete", evt => {
                                newDirLine.remove();
                                SemRepeater.deleteCustomDir(
                                    this.currentEle, dir
                                );
                            });
                        }
                    });
                }
            }

            const pathDiv = this.htmlSemanticRepeatCard
                .querySelector("div[data-repeat-type=\"path\"]");
            const definePathBtn = pathDiv
                .querySelector("#repeat-define-path-btn");
            const pathLine = pathDiv.querySelector("#repeat-path-line");
            if (path) {
                configuredRepeatTypes.push("path");
                definePathBtn.classList.add("d-none");
                pathLine.classList.remove("d-none");
                const pathCoordinatesDiv = pathDiv
                    .querySelector("#repeat-path-coordinates-div");
                pathCoordinatesDiv.innerHTML = path.array().join();
            }
            else {
                definePathBtn.classList.remove("d-none");
                pathLine.classList.add("d-none");
            }
            const rotationCheckbox = this.htmlSemanticRepeatCard
                .querySelector("#repeat-along-path-with-rotation");
            rotationCheckbox.checked = rotateAlongPath;

            const shownRepeatTypes = [];
            if (configuredRepeatTypes.includes("path")) {
                shownRepeatTypes.push("path");
            }
            else if (configuredRepeatTypes.includes("grid")) {
                shownRepeatTypes.push("grid");
            }
            else {
                if (configuredRepeatTypes.includes("directions")) {
                    shownRepeatTypes.push("directions");
                }
                if (configuredRepeatTypes.includes("direction")) {
                    shownRepeatTypes.push("direction");
                }
            }

            const repeatTypeBtns = this.htmlSemanticRepeatCard
                .querySelector("#inspector-repeat-types-btn-group")
                .querySelectorAll("button");
            repeatTypeBtns.forEach(btn => {
                const btnRepeatType = btn.dataset.repeatType;
                const typeDiv = this.htmlSemanticRepeatCard
                    .querySelector(`div[data-repeat-type="${btnRepeatType}"]`);
                if (shownRepeatTypes.includes(btnRepeatType)) {
                    btn.classList.replace(
                        "btn-outline-secondary",
                        "btn-outline-primary"
                    );
                    if (typeDiv) {
                        typeDiv.classList.remove("d-none");
                    }
                }
                else {
                    btn.classList.replace(
                        "btn-outline-primary",
                        "btn-outline-secondary"
                    );
                    if (typeDiv) {
                        typeDiv.classList.add("d-none");
                    }
                }
            });

            const marginDiv = this.htmlSemanticRepeatCard
                .querySelector("#repeat-margin-div");
            const marginInput = marginDiv
                .querySelector(".repeat-margin");
            marginInput.value = margin;

            const testIntersectionsDiv = this.htmlSemanticRepeatCard
                .querySelector("#repeat-test-intersections-div");
            const testIntersectionsCheckbox = testIntersectionsDiv
                .querySelector("#repeat-test-intersections");
            testIntersectionsCheckbox.checked = testIntersections;

            if (shownRepeatTypes.length > 0) {
                marginDiv.classList.remove("d-none");
                testIntersectionsDiv.classList.remove("d-none");
            }
            else {
                marginDiv.classList.add("d-none");
                testIntersectionsDiv.classList.add("d-none");
            }
        }

        const stretchable = SemStretcher.isSemStretchable(element);
        this.htmlSemanticStretchCard.hidden = !stretchable;
        if (stretchable) {
            const {
                horizontalFactor,
                verticalFactor
            } = SemStretcher.getSemStretchableConfigOfElement(element);
            const stretchHorizontalFactorInput = this.htmlSemanticStretchCard
                .querySelector("#stretch-factor-horizontal");
            const stretchVerticalFactorInput = this.htmlSemanticStretchCard
                .querySelector("#stretch-factor-vertical");
            stretchHorizontalFactorInput.value = horizontalFactor;
            stretchVerticalFactorInput.value = verticalFactor;
        }

        const anchorable = SemAnchorer.isSemAnchorable(element);
        this.htmlSemanticAnchorCard.hidden = !anchorable;
        if (anchorable) {
            const { anchorPoint } = SemAnchorer
                .getSemAnchorableConfigOfElement(element);
            const defineAnchorBtn = this.htmlSemanticAnchorCard
                .querySelector("#anchor-define-anchor-point-btn");
            const anchorPointLine = this.htmlSemanticAnchorCard
                .querySelector("#anchor-point-line");
            if (anchorPoint) {
                defineAnchorBtn.classList.add("d-none");
                anchorPointLine.classList.remove("d-none");
                const anchorCoordinatesSpan = anchorPointLine
                    .querySelector("#anchor-point-coordinates");
                anchorCoordinatesSpan.innerHTML = `
                    (${anchorPoint.x.toFixed(2)}, ${anchorPoint.y.toFixed(2)})
                `;
            }
            else {
                defineAnchorBtn.classList.remove("d-none");
                anchorPointLine.classList.add("d-none");
            }
        }

        this.currentEle = element;
        this.htmlInspector.hidden = false;
    }

    onSemRepeatTypeButtonClick(button) {
        if (!this.repeatable) {
            return;
        }

        const activeRepeatTypeBtns = this.htmlSemanticRepeatCard
            .querySelector("#inspector-repeat-types-btn-group")
            .querySelectorAll(".btn-outline-primary");
        const { repeatType } = button.dataset;
        // query for div that belongs to repeat type
        const typeDiv = this.htmlSemanticRepeatCard
            .querySelector(`div[data-repeat-type="${repeatType}"]`);

        if (Array.from(activeRepeatTypeBtns).includes(button)) {
            button.classList.replace(
                "btn-outline-primary",
                "btn-outline-secondary"
            );
            typeDiv.classList.add("d-none");
            if (activeRepeatTypeBtns.length === 1) {
                const marginDiv = this.htmlSemanticRepeatCard
                    .querySelector("#repeat-margin-div");
                const testIntersectionsDiv = this.htmlSemanticRepeatCard
                    .querySelector("#repeat-test-intersections-div");
                marginDiv.classList.add("d-none");
                testIntersectionsDiv.classList.add("d-none");
                SemRepeater.removeRepeatableBehaviour(this.currentEle);
            }
            this.deactivateRepeatType(repeatType);
        }
        else {
            activeRepeatTypeBtns.forEach(btn => {
                const btnRepeatType = btn.dataset.repeatType;
                if (btnRepeatType === "directions"
                    && repeatType === "direction"
                    || btnRepeatType === "direction"
                    && repeatType === "directions") {
                    return;
                }
                btn.classList.replace(
                    "btn-outline-primary",
                    "btn-outline-secondary"
                );
                const btnTypeDiv = this.htmlSemanticRepeatCard
                    .querySelector(
                        `div[data-repeat-type="${btnRepeatType}"]`
                    );
                btnTypeDiv.classList.add("d-none");
                this.deactivateRepeatType(btnRepeatType);
            });

            button.classList.replace(
                "btn-outline-secondary",
                "btn-outline-primary"
            );
            typeDiv.classList.remove("d-none");

            if (repeatType === "grid") {
                SemRepeater.activateRepeatTypeGrid(this.currentEle);
            }
        }
    }

    deactivateRepeatType(repeatType) {
        switch (repeatType) {
        case "directions":
            SemRepeater.deactivateRepeatTypeDirections(this.currentEle);
            break;
        case "grid":
            SemRepeater.deactivateRepeatTypeGrid(this.currentEle);
            break;
        case "direction":
            SemRepeater
                .deactivateRepeatTypeCustomDirections(this.currentEle);
            break;
        case "path":
            SemRepeater.deactivateRepeatTypePath(this.currentEle);
            break;
        default:
            break;
        }
    }
}
