export default class CustomDirLine extends HTMLElement {
    constructor() {
        super();

        this.innerHTML = `
            <div class="d-flex justify-content-between mb-1">
                <span class="fas fa-long-arrow-alt-right align-self-center" style="transform: rotate(${this.angle}deg)"></span>
                <span>${this.angle.toFixed(2)}°</span>
                <input id="amount-input-field" type="number" value="${this.amount}" min="-1" max="100" style="width: 40px">
                <button id="delete-button" type="button" class="btn btn-sm btn-outline-secondary"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-trigger="hover" title="Delete custom direction">
                    <span class="fas fa-trash-alt"></span>
                </button>
            </div>
        `;

        this.querySelector("#amount-input-field")
            .addEventListener(
                "change",
                event => {
                    this.amount = event.target.value;
                }
            );

        this.querySelector("#delete-button")
            .addEventListener("click", evt => {
                this.dispatchEvent(new Event("delete"));
            });
    }

    get angle() {
        const angleString = this.getAttribute("angle");

        if (angleString === null || angleString === "") {
            return 0;
        }

        return parseFloat(angleString);
    }

    get amount() {
        const amountString = this.getAttribute("amount");

        if (amountString === null || amountString === "") {
            return -1;
        }

        return parseInt(amountString, 10);
    }

    set amount(amt) {
        const currentAmount = this.amount;
        const newAmount = Math.max(-1, amt);

        if (currentAmount === newAmount) {
            return;
        }

        const amtStr = newAmount.toString();

        this.setAttribute("amount", amtStr);
        this.querySelector("#amount-input-field").value = amtStr;

        this.dispatchEvent(new Event("amount-changed"));
    }
}

window.customElements.define("custom-dir-line", CustomDirLine);
