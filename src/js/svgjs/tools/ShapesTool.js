import Tool from "./Tool";
import { SvgElementCreated } from "../../events/SvgElementEvents";

export const ShapesEnum = Object.freeze({
    rectangle: "rect",
    circle: "circle",
    ellipse: "ellipse"
});

export default class ShapesTool extends Tool {
    constructor(svgEle, draw, defaultShape = ShapesEnum.rectangle) {
        super(svgEle, draw);

        this.defaultShape = defaultShape;
        this.shape = defaultShape;
        this.createdShape = null;
        this.startPoint = [];
        this.endPoint = [];
    }

    reset(draw, svgEle) {
        super.reset(draw, svgEle);
        this.shape = this.defaultShape;
    }

    setshape(shape) {
        this.shape = shape;
    }

    activate() {
        super.activate();
        this.listenerHandler.on("mousedown", this.onMouseDown);
    }

    onMouseDown(event) {
        this.startCreateElement(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );
        this.isDrawing = true;

        this.listenerHandler.on("mousemove", this.onMouseMove);
        this.intervalId = setInterval(
            this.continueActionInInterval.bind(this),
            50
        );
        this.listenerHandler.on("mouseup", this.onMouseUp);
    }

    startCreateElement(x, y) {
        this.startPoint = this.pageCoordToCanvasCoord(x, y);
        this.endPoint = this.startPoint;

        const { width, height } = this.calcWidthAndHeight(
            this.startPoint,
            this.endPoint
        );
        this.createdShape = this.draw[this.shape](width, height);

        this.createdShape.move(this.startPoint.x, this.startPoint.y);
        this.applyStylingToElement(this.createdShape);
    }

    onMouseMove(event) {
        this.endPoint = this.pageCoordToCanvasCoord(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );
    }

    continueActionInInterval() {
        this.updateSizeOfCreatedShape();
        this.moveShapeToSizeRelatedPos();
    }

    onMouseUp(event) {
        this.listenerHandler.removeListener("mouseup");
        this.listenerHandler.removeListener("mousemove");
        clearInterval(this.intervalId);

        this.finishCreateElement();
    }

    finishCreateElement() {
        this.updateSizeOfCreatedShape();
        this.moveShapeToSizeRelatedPos();

        this.createdShape.node
            .dispatchEvent(new SvgElementCreated(this.createdShape));
    }

    pageCoordToCanvasCoord(pageX, pageY) {
        return this.draw.point(pageX, pageY);
    }

    calcWidthAndHeight(startPoint, endPoint) {
        let w = 0;
        let h = 0;

        if (startPoint.x < endPoint.x) {
            w = endPoint.x - startPoint.x;
        }
        else {
            w = startPoint.x - endPoint.x;
        }

        if (startPoint.y < endPoint.y) {
            h = endPoint.y - startPoint.y;
        }
        else {
            h = startPoint.y - endPoint.y;
        }

        return { width: w, height: h };
    }

    // I thought this was necessary for circle, but apparently it is not
    calcDiameter(startPoint, endPoint) {
        let xs = 0;
        let ys = 0;

        xs = endPoint.x - startPoint.x;
        xs *= xs;

        ys = endPoint.Y - startPoint.Y;
        ys *= ys;

        return Math.sqrt(xs + ys);
    }

    updateSizeOfCreatedShape() {
        const { width, height } = this.calcWidthAndHeight(
            this.startPoint,
            this.endPoint
        );
        this.createdShape.size(width, height);
    }

    calcSizeRelatedPos(startPoint, endPoint) {
        let w = 0;
        let h = 0;
        let mx = startPoint.x;
        let my = startPoint.y;

        if (startPoint.x < endPoint.x) {
            w = endPoint.x - startPoint.x;
            mx = endPoint.x - w;
        }
        else {
            w = startPoint.x - endPoint.x;
            mx = startPoint.x - w;
        }

        if (startPoint.y < endPoint.y) {
            h = endPoint.y - startPoint.y;
            my = endPoint.y - h;
        }
        else {
            h = startPoint.y - endPoint.y;
            my = startPoint.y - h;
        }

        return { x: mx, y: my };
    }

    moveShapeToSizeRelatedPos() {
        const { x, y } = this.calcSizeRelatedPos(
            this.startPoint,
            this.endPoint
        );
        this.createdShape.move(x, y);
    }
}
