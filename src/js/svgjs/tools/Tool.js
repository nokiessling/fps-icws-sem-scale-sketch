import ListenerHandler from "../../helper/ListenerHandler";

export default class Tool {
    constructor(svgEle, draw) {
        this.svgEle = svgEle;
        this.draw = draw;
        this.listenerHandler = new ListenerHandler(
            this,
            this.svgEle,
            "addEventListener",
            "removeEventListener"
        );
    }

    reset(draw, svgEle) {
        this.listenerHandler.removeAllListeners();
        this.svgEle = svgEle;
        this.draw = draw;
        this.listenerHandler = new ListenerHandler(
            this,
            this.svgEle,
            "addEventListener",
            "removeEventListener"
        );
    }

    activate() {}

    deactivate() {
        this.listenerHandler.removeAllListeners();
    }

    applyStylingToElement(svgElement, dashed = false) {
        svgElement.fill("none");
        svgElement.stroke({
            color: "#222",
            width: 2,
            linecap: "round",
            linejoin: "round",
        });
        if (dashed) {
            svgElement.stroke({ dasharray: "10,10" });
        }
    }
}
