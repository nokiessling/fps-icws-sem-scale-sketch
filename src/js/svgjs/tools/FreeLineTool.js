import simplify from "simplify-js";
import "@svgdotjs/svg.draggable.js";
import Tool from "./Tool";
import { SvgElementCreated } from "../../events/SvgElementEvents";

export default class FreeLineTool extends Tool {
    constructor(svgEle, draw) {
        super(svgEle, draw);
        this.points = [];
        this.polyline = null;
    }

    activate() {
        super.activate();
        this.listenerHandler.on("mousedown", this.onMouseDown);
    }

    onMouseDown(event) {
        this.startCreateElement(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );

        this.listenerHandler.on("mousemove", this.onMouseMove);
        this.intervalId = setInterval(
            this.continueActionInInterval.bind(this),
            50
        );
        this.listenerHandler.on("mouseup", this.onMouseUp);
    }

    startCreateElement(x, y) {
        this.points = [];
        this.addCurrentPoint(x, y);
        this.polyline = this.draw.polyline(this.points);
        this.applyStylingToElement(this.polyline);
    }

    onMouseMove(event) {
        this.addCurrentPoint(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );
    }

    continueActionInInterval() {
        this.polyline.plot(this.points.map(p => [p.x, p.y]));
    }

    onMouseUp(event) {
        this.listenerHandler.removeListener("mouseup");
        this.listenerHandler.removeListener("mousemove");
        clearInterval(this.intervalId);

        this.finishCreateElement();
    }

    finishCreateElement() {
        this.polyline.plot(simplify(this.points, 1)
            .map(p => [p.x, p.y]));

        this.polyline.node.dispatchEvent(new SvgElementCreated(this.polyline));
    }

    addCurrentPoint(pageX, pageY) {
        const drawPoint = this.draw.point(pageX, pageY);
        this.points.push({
            x: drawPoint.x,
            y: drawPoint.y,
        });
    }
}
