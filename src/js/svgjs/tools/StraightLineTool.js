import Tool from "./Tool";
import { SvgElementCreated } from "../../events/SvgElementEvents";

export default class StraightLineTool extends Tool {
    constructor(svgEle, draw) {
        super(svgEle, draw);

        this.line = null;
        this.startPoint = [];
        this.endPoint = [];
        this.isDrawingLine = false;
    }

    activate() {
        super.activate();
        this.listenerHandler.on("mousedown", this.onMouseDown);
    }

    deactivate() {
        super.deactivate();
        // in case line wasn't finished when selecting another tool, finish it
        if (this.isDrawingLine) {
            this.finishDrawing();
        }
    }

    onMouseDown(event) {
        if (!this.isDrawingLine) {
            this.startDrawing(
                event.pageX + window.pageXOffset,
                event.pageY - window.pageYOffset
            );
        }
        else {
            this.finishDrawing();
        }
    }

    startDrawing(pageX, pageY, dashed = false, withMarker = false) {
        this.startCreateElement(pageX, pageY, dashed, withMarker);
        this.isDrawingLine = true;

        this.listenerHandler.on("mousemove", this.onMouseMove);
        this.intervalId = setInterval(
            this.continueActionInInterval.bind(this),
            50
        );
    }

    finishDrawing() {
        this.listenerHandler.removeListener("mousemove");
        clearInterval(this.intervalId);

        this.finishCreateElement();
        this.isDrawingLine = false;
    }

    startCreateElement(x, y, dashed = false, withMarker = false) {
        this.startPoint = this.pageCoordToCanvasCoord(x, y);
        this.endPoint = this.startPoint;
        this.line = this.draw.line([this.startPoint, this.endPoint]);
        this.applyStylingToElement(this.line, dashed);
        if (withMarker) {
            this.line.marker("end", 3, 3, add => {
                add.circle(3).fill("#222");
            });
        }
    }

    onMouseMove(event) {
        this.endPoint = this.pageCoordToCanvasCoord(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );
    }

    continueActionInInterval() {
        this.line.plot([this.startPoint, this.endPoint]);
    }

    finishCreateElement() {
        this.line.plot([this.startPoint, this.endPoint]);

        this.line.node.dispatchEvent(new SvgElementCreated(this.line));
    }

    pageCoordToCanvasCoord(pageX, pageY) {
        const drawPoint = this.draw.point(pageX, pageY);
        return [drawPoint.x, drawPoint.y];
    }
}
