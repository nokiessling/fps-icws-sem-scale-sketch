import Tool from "./Tool";
import { SvgElementCreated } from "../../events/SvgElementEvents";

export default class PathTool extends Tool {
    constructor(svgEle, draw, closePath = false) {
        super(svgEle, draw);

        this.path = null;
        this.points = [];
        this.previewLine = null;
        this.previewStartPoint = null;
        this.previewEndPoint = null;
        this.isDrawing = false;
        this.closePath = closePath;
    }

    activate() {
        super.activate();
        this.listenerHandler.on("mousedown", this.onMouseDown);
    }

    deactivate() {
        super.deactivate();
        // in case path wasn't finished when selecting another tool, finish it
        if (this.isDrawing) {
            this.listenerHandler.removeListener("mousemove");
            clearInterval(this.intervalId);
            this.finishCreateElement();
            this.isDrawing = false;
        }
    }

    onMouseDown(event) {
        const pageXWithOffset = event.pageX + window.pageXOffset;
        const pageYWithOffset = event.pageY - window.pageYOffset;
        const currPoint = this.pageCoordToCanvasCoord(
            pageXWithOffset, pageYWithOffset
        );
        if (!this.isDrawing) {
            this.startDrawing(pageXWithOffset, pageYWithOffset);
        }
        else if (this.points.some(
            p => p.x === currPoint.x && p.y === currPoint.y
        )) {
            this.listenerHandler.removeListener("mousemove");
            clearInterval(this.intervalId);

            this.isDrawing = false;
            this.finishCreateElement();
        }
        else {
            this.addCurrentPoint(pageXWithOffset, pageYWithOffset);
            this.updatePreviewStartPoint(pageXWithOffset, pageYWithOffset);
            this.path.plot(this.buildPathString(this.points));
        }
    }

    startDrawing(x, y) {
        this.startCreateElement(x, y);
        this.startCreatePreview();
        this.isDrawing = true;
    }

    startCreateElement(x, y) {
        this.points = [];
        this.addCurrentPoint(x, y);
        this.path = this.draw.path(this.buildPathString(this.points));
        this.applyStylingToElement(this.path);
    }

    startCreatePreview() {
        if (this.points.length === 0) {
            console.log("Cannot create preview, no points selected yet. "
                + "Please call startCreateElement before startCreatePreview.");
            return;
        }
        const { x, y } = this.points[this.points.length - 1];
        this.previewStartPoint = [x, y];
        this.previewEndPoint = this.previewStartPoint;
        this.previewLine = this.draw.line(
            [this.previewStartPoint, this.previewEndPoint]
        );
        this.applyStylingToElement(this.previewLine);

        this.listenerHandler.on("mousemove", this.onMouseMove);
        this.intervalId = setInterval(
            this.continueActionInInterval.bind(this),
            50
        );
    }

    onMouseMove(event) {
        const { x, y } = this.pageCoordToCanvasCoord(
            event.pageX + window.pageXOffset,
            event.pageY - window.pageYOffset
        );
        this.previewEndPoint = [x, y];
    }

    continueActionInInterval() {
        this.previewLine.plot([this.previewStartPoint, this.previewEndPoint]);
    }

    finishCreateElement() {
        if (this.previewLine) {
            this.previewLine.remove();
            this.previewLine = null;
        }
        this.previewStartPoint = null;
        this.previewEndPoint = null;

        this.path.plot(this.buildPathString(this.points));
        this.path.node.dispatchEvent(new SvgElementCreated(this.path));
    }

    pageCoordToCanvasCoord(pageX, pageY) {
        const drawPoint = this.draw.point(pageX, pageY);
        return { x: drawPoint.x, y: drawPoint.y };
    }

    addCurrentPoint(pageX, pageY) {
        const drawPoint = this.pageCoordToCanvasCoord(pageX, pageY);
        this.points.push({
            x: drawPoint.x,
            y: drawPoint.y,
        });
    }

    updatePreviewStartPoint(pageX, pageY) {
        const { x, y } = this.pageCoordToCanvasCoord(pageX, pageY);
        this.previewStartPoint = [x, y];
    }

    buildPathString(points) {
        let pathString = "M " + points[0].x + " " + points[0].y;
        for (let i = 1; i < points.length; i++) {
            pathString += " L " + points[i].x + " " + points[i].y;
        }
        if (this.closePath) {
            pathString += " z";
        }
        return pathString;
    }
}
