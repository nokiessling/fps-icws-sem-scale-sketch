export default class ToolManager {
    static Instance;

    constructor(tools, defaultToolName) {
        if (ToolManager.Instance) {
            ToolManager.Instance.deactivate();
        }
        ToolManager.Instance = this;
        this.tools = new Map(tools);
        this.defaultToolName = defaultToolName;
        this.selectedToolName = null;
        this.selectedTool = null;
        this.selectTool(defaultToolName);
        this.active = true;
    }

    reset(draw, svgEle) {
        if (this.selectedTool) {
            this.selectedTool.deactivate();
            this.selectedTool = null;
        }
        Array.from(this.tools.values()).forEach(tool => {
            tool.reset(draw, svgEle);
        });
        this.selectTool(this.defaultToolName);
        this.active = true;
    }

    activate() {
        if (this.active) {
            return;
        }

        this.selectedTool.activate();
        this.active = true;
    }

    deactivate() {
        if (!this.active) {
            return;
        }

        this.selectedTool.deactivate();
        this.active = false;
    }

    selectTool(toolName) {
        if (this.selectedTool) {
            this.selectedTool.deactivate();
        }
        const tool = this.getToolByName(toolName);
        if (tool) {
            this.selectedToolName = toolName;
            this.selectedTool = tool;
            this.selectedTool.activate();
        }
        else {
            console.log("ToolManager: Could not select tool with name ",
                toolName, "! No such tool known.");
        }
    }

    getToolByName(toolName) {
        return this.tools.get(toolName);
    }
}
