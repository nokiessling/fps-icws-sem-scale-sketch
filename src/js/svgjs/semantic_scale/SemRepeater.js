import Vector2D from "../../math/Vector2D";
import { SvgElementCreated } from "../../events/SvgElementEvents";
import ToolManager from "../tools/ToolManager";

export default class SemRepeater {
    static vectorMap = new Map([
        ["top_left", new Vector2D(-1, -1)],
        ["top", new Vector2D(0, -1)],
        ["top_right", new Vector2D(1, -1)],
        ["left", new Vector2D(-1, 0)],
        ["grid", new Vector2D(0, 0)],
        ["right", new Vector2D(1, 0)],
        ["bottom_left", new Vector2D(-1, 1)],
        ["bottom", new Vector2D(0, 1)],
        ["bottom_right", new Vector2D(1, 1)],
    ])

    static addRepeatableBehaviour(element) {
        element.semRepeatable();
    }

    static removeRepeatableBehaviour(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.semRepeatable(false);
    }

    static isSemRepeatable(element) {
        return element.isSemRepeatable();
    }

    static getSemRepeatableConfigOfElement(element) {
        return element.semRepeatableConfig();
    }

    static activateRepeatTypeDirections(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        const selectedDirections = element
            .semRepeatableConfig()
            .directions;

        // remove grid if grid is configured
        if (selectedDirections !== undefined
            && selectedDirections.length > 0) {
            const index = selectedDirections.findIndex(
                value => value.x === 0 && value.y === 0
            );
            if (index > -1) {
                selectedDirections.splice(index, 1);
            }
        }

        // update repeatable config to actually remove grid and path
        element.updateSemRepeatable(
            selectedDirections,
            undefined,
            null
        );
    }

    static deactivateRepeatTypeDirections(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        const selectedDirections = element
            .semRepeatableConfig()
            .directions;

        if (selectedDirections === undefined
            || selectedDirections.length === 0) {
            return;
        }

        Array.from(this.vectorMap.values())
            .forEach(vec => {
                // don't remove grid or custom direction
                if (vec.x === 0 && vec.y === 0
                    || vec.x > -1 && vec.x < 0
                    || vec.x > 0 && vec.x < 1
                    || vec.y > -1 && vec.y < 0
                    || vec.y > 0 && vec.y < 1) {
                    return;
                }
                // remove predefined direction if it was configured
                const index = selectedDirections.findIndex(
                    value => vec.x === value.x && vec.y === value.y
                );
                if (index > -1) {
                    selectedDirections.splice(index, 1);
                }
            });

        element.updateSemRepeatable(selectedDirections);
    }

    static activateRepeatTypeGrid(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        let selectedDirections = element
            .semRepeatableConfig()
            .directions;

        // add grid to configured directions
        if (selectedDirections !== undefined) {
            selectedDirections.push(new Vector2D(0, 0));
        }
        else {
            selectedDirections = [new Vector2D(0, 0)];
        }

        // update repeatable config to actually add grid and remove path
        element.updateSemRepeatable(
            selectedDirections,
            undefined,
            null
        );
    }

    static deactivateRepeatTypeGrid(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        const selectedDirections = element
            .semRepeatableConfig()
            .directions;

        if (selectedDirections === undefined
            || selectedDirections.length === 0) {
            return;
        }

        // remove grid if grid is configured
        const index = selectedDirections.findIndex(
            value => value.x === 0 && value.y === 0
        );
        if (index > -1) {
            selectedDirections.splice(index, 1);
        }

        // update repeatable config to actually remove grid
        element.updateSemRepeatable(selectedDirections);
    }

    static activateRepeatTypeCustomDirections(element) {
        this.activateRepeatTypeDirections(element);
    }

    static deactivateRepeatTypeCustomDirections(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        let configuredDirections = element.semRepeatableConfig().directions;

        configuredDirections = configuredDirections.filter(
            dir => !(Array.from(this.vectorMap.values())
                .find(vec => dir.x === vec.x && dir.y === vec.y))
        );

        element.updateSemRepeatable(configuredDirections);
    }

    static activateRepeatTypePath(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        // update repeatable config to remove all configured directions
        element.updateSemRepeatable(
            [],
            undefined,
            undefined,
            undefined,
            new Map([
                [new Vector2D(-1, -1), -1],
                [new Vector2D(0, -1), -1],
                [new Vector2D(1, -1), -1],
                [new Vector2D(-1, 0), -1],
                [new Vector2D(1, 0), -1],
                [new Vector2D(-1, 1), -1],
                [new Vector2D(0, 1), -1],
                [new Vector2D(1, 1), -1],
            ])
        );
    }

    static deactivateRepeatTypePath(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            undefined,
            null
        );
    }

    static activateAddingOfCustomDirection(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        ToolManager.Instance.activate();
        const currentTool = ToolManager.Instance.selectedToolName;
        ToolManager.Instance.selectTool("straightLine");

        const eleRect = element.node.getBoundingClientRect();
        const eleCenter = new Vector2D(
            eleRect.x + eleRect.width / 2,
            eleRect.y + eleRect.height / 2
        );
        ToolManager.Instance.selectedTool.startDrawing(
            eleCenter.x,
            eleCenter.y
        );

        const onLineCreatedEvt = evt => {
            evt.stopImmediatePropagation();

            let {
                directions,
                amount
            } = element
                .semRepeatableConfig();

            let { endPoint } = ToolManager.Instance.selectedTool;
            const svgEleRect = document.querySelector("svg")
                .getBoundingClientRect();
            endPoint = new Vector2D(
                endPoint[0] + svgEleRect.x,
                endPoint[1] + svgEleRect.y
            );
            const direction = new Vector2D(
                endPoint.x - eleCenter.x,
                endPoint.y - eleCenter.y
            );
            direction.normalize();

            if (directions !== undefined) {
                directions.push(direction);
            }
            else {
                directions = [direction];
            }

            amount.set(direction, -1);
            element.updateSemRepeatable(
                directions,
                undefined,
                undefined,
                undefined,
                amount
            );
            evt.instance.remove();

            ToolManager.Instance.selectTool(currentTool);
            ToolManager.Instance.deactivate();
        };

        document.addEventListener(
            SvgElementCreated.name,
            onLineCreatedEvt,
            {
                once: true,
                capture: true
            }
        );
    }

    static activateAddingOfPath(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        ToolManager.Instance.activate();
        const currentTool = ToolManager.Instance.selectedToolName;
        ToolManager.Instance.selectTool("path");

        const eleRect = element.node.getBoundingClientRect();
        const eleCenter = new Vector2D(
            eleRect.x + eleRect.width / 2,
            eleRect.y + eleRect.height / 2
        );
        ToolManager.Instance.selectedTool.startDrawing(
            eleCenter.x,
            eleCenter.y
        );

        const onPathCreatedEvt = evt => {
            evt.stopImmediatePropagation();
            element.updateSemRepeatable(
                undefined,
                undefined,
                evt.instance
            );
            ToolManager.Instance.selectTool(currentTool);
            ToolManager.Instance.deactivate();
        };

        document.addEventListener(
            SvgElementCreated.name,
            onPathCreatedEvt,
            {
                once: true,
                capture: true
            }
        );
    }

    static toggleRepeatDir(element, dirStr) {
        if (!element.isSemRepeatable()) {
            return;
        }

        const selectedDirections = element
            .semRepeatableConfig()
            .directions;
        const repeatVec = this.vectorMap.get(dirStr);
        const index = selectedDirections.findIndex(
            value => repeatVec.x === value.x && repeatVec.y === value.y
        );
        if (index > -1) {
            selectedDirections.splice(index, 1);
        }
        else {
            selectedDirections.push(repeatVec);
        }

        element.updateSemRepeatable(selectedDirections);
    }

    static updateAmountOfDir(element, dir, newAmount, removeDirIfZero = false) {
        if (!element.isSemRepeatable()) {
            return;
        }
        if (dir === null) {
            return;
        }

        const {
            directions,
            amount
        } = element.semRepeatableConfig();
        const key = this.getDirKeyInAmountMap(amount, dir);

        if (key !== undefined && amount.get(key) === newAmount) {
            return;
        }

        const gridIndex = directions.findIndex(
            value => value.x === 0 && value.y === 0
        );

        // if repeat in grid is not configured
        if (gridIndex < 0) {
            const index = directions.findIndex(
                value => dir.x === value.x && dir.y === value.y
            );

            // if direction is configured and repeat amount is set to zero,
            // remove dir
            if (index > -1 && removeDirIfZero && newAmount === 0) {
                directions.splice(index, 1);
            }
            // if direction is not configured yet and an amount other than zero
            // is set, add direction
            else if (index < 0
                && (!removeDirIfZero || (removeDirIfZero && newAmount !== 0))) {
                directions.push(dir);
            }
        }

        if (key !== undefined) {
            if (removeDirIfZero
                && newAmount === 0
                && !(Array.from(this.vectorMap.values())
                    .find(vec => dir.x === vec.x && dir.y === vec.y))) {
                amount.remove(key);
            }
            else {
                amount.set(key, newAmount);
            }
        }
        else {
            amount.set(dir, newAmount);
        }

        element.updateSemRepeatable(
            directions,
            undefined,
            undefined,
            undefined,
            amount,
            undefined
        );
    }

    static updateRepeatSymmetrically(element, repeatSymmetrically) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            repeatSymmetrically
        );
    }

    static updateRotateAlongPath(element, rotate) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            undefined,
            undefined,
            rotate,
            undefined,
            undefined
        );
    }

    static updateMargin(element, newMargin) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            newMargin
        );
    }

    static updateTestIntersections(element, testIntersections) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            testIntersections
        );
    }

    static deleteCustomDir(element, dir) {
        if (!element.isSemRepeatable()) {
            return;
        }

        const {
            directions,
            amount
        } = element
            .semRepeatableConfig();

        const index = directions.findIndex(
            value => dir.x === value.x && dir.y === value.y
        );
        if (index > -1) {
            directions.splice(index, 1);
            amount.delete(this.getDirKeyInAmountMap(amount, dir));
        }

        element.updateSemRepeatable(
            directions,
            undefined,
            undefined,
            undefined,
            amount
        );
    }

    static deletePath(element) {
        if (!element.isSemRepeatable()) {
            return;
        }

        element.updateSemRepeatable(
            undefined,
            undefined,
            null
        );
    }

    static getDirKeyInAmountMap(amountMap, direction) {
        return Array.from(amountMap.keys())
            .find(vec => vec.x === direction.x && vec.y === direction.y);
    }
}
