import {
    Element, extend, Svg,
} from "@svgdotjs/svg.js";
import {
    SvgElementDeleted,
    SvgElementMetadataChanged,
    SvgElementParentChanged
} from "../../events/SvgElementEvents";
import ObserverHandler from "../../helper/ObserverHandler";
import ListenerHandler from "../../helper/ListenerHandler";

class SemStretchableHandler {
    constructor(el, horizontalFactor, verticalFactor, updatePos) {
        el.remember("_sem_stretchable", this);
        this.enabled = false;
        this.el = el;
        this.horizontalFactor = horizontalFactor;
        this.verticalFactor = verticalFactor;
        this.updatePos = updatePos;
        this.semParent = null;
        this.prevParentWidth = null;
        this.prevParentHeight = null;
        this.partXOfParentWidth = null;
        this.partYOfParentHeight = null;
        this.stretchMarker = null;
        this.elObserverHandler = new ObserverHandler(
            this.el,
            () => {
                this.updateMarker();
            }
        );
        this.parentObserverHandler = null;

        this.listenerHandler = new ListenerHandler(
            this,
            this.el.node,
            "addEventListener",
            "removeEventListener"
        );
    }

    init(enable = true) {
        this.enabled = enable;
        if (enable) {
            this.semParent = this.getSemParent();
            this.prevParentWidth = this.semParent.width();
            this.prevParentHeight = this.semParent.height();
            this.calcPartsOfParentWidthAndHeight();
            this.addObserverToParent();

            this.listenerHandler.on(SvgElementParentChanged.name, evt => {
                if (evt.instance !== this.el) {
                    return;
                }
                this.onParentChange(evt.newParent);
            });

            this.createMarker();
            this.elObserverHandler.addObserver();

            this.listenerHandler.on(SvgElementDeleted.name, evt => {
                if (evt.instance === this.el) {
                    this.removeMarker();
                }
            });
        }
        else {
            this.listenerHandler.removeAllListeners();
            if (this.parentObserverHandler) {
                this.parentObserverHandler.removeObserver();
            }

            this.removeMarker();
            this.elObserverHandler.removeObserver();
        }
    }

    getSemParent() {
        const parent = this.el.parent();

        if (parent instanceof Svg && parent.isRoot()) {
            return parent;
        }

        const parentOfParent = parent.parent();
        const parentId = parent.data("children-from-node-id");

        return Array.from(
            parentOfParent.children()
        ).find(
            child => child.data("node-id") === parentId
        );
    }

    addObserverToParent() {
        this.parentObserverHandler = new ObserverHandler(
            this.semParent,
            () => {
                this.stretch();
            }
        );
        this.parentObserverHandler.addObserver();
    }

    stretch() {
        const currParentWidth = this.semParent.width();
        const currParentHeight = this.semParent.height();

        if (currParentWidth === this.prevParentWidth
            && currParentHeight === this.prevParentHeight) {
            return;
        }

        const parentWidthDiff = currParentWidth - this.prevParentWidth;
        const parentHeightDiff = currParentHeight - this.prevParentHeight;

        const widthFactor = parentWidthDiff / this.prevParentWidth;
        const heightFactor = parentHeightDiff / this.prevParentHeight;

        const newWidth = this.el.width()
            + widthFactor * this.horizontalFactor * this.el.width();
        const newHeight = this.el.height()
            + heightFactor * this.verticalFactor * this.el.height();

        this.el.width(newWidth);
        this.el.height(newHeight);

        if (this.updatePos) {
            const newXPos = this.semParent.x() + this.partXOfParentWidth
                * currParentWidth;
            const newYPos = this.semParent.y() + this.partYOfParentHeight
                * currParentHeight;

            this.el.x(newXPos);
            this.el.y(newYPos);
        }

        this.prevParentWidth = currParentWidth;
        this.prevParentHeight = currParentHeight;
    }

    onParentChange(newParent) {
        // remove listeners from old parent
        if (this.parentObserverHandler) {
            this.parentObserverHandler.removeObserver();
        }
        this.semParent = newParent;
        this.prevParentWidth = this.semParent.width();
        this.prevParentHeight = this.semParent.height();
        this.calcPartsOfParentWidthAndHeight();
        this.addObserverToParent();
    }

    calcPartsOfParentWidthAndHeight() {
        const diffXPos = this.el.x() - this.semParent.x();
        const diffYPos = this.el.y() - this.semParent.y();
        this.partXOfParentWidth = diffXPos / this.prevParentWidth;
        this.partYOfParentHeight = diffYPos / this.prevParentHeight;
    }

    createMarker() {
        if (this.stretchMarker) {
            this.removeMarker();
        }

        this.stretchMarker = document.createElement("div");
        this.stretchMarker.style.position = "absolute";
        this.stretchMarker.style.pointerEvents = "none";
        this.stretchMarker.innerHTML = `
                <span class="fas fa-compress" style='color: lightgrey'></span>
            `;
        document.querySelector("#sem-behaviour-marker")
            .appendChild(this.stretchMarker);
        this.updateMarker();
    }

    updateMarker() {
        if (!this.stretchMarker) {
            return;
        }

        const eleRect = this.el.node.getBoundingClientRect();
        const markerRect = this.stretchMarker.getBoundingClientRect();
        const posX = eleRect.x + eleRect.width + window.pageXOffset;
        const posY = eleRect.y - markerRect.height - 3 + window.pageYOffset;

        this.stretchMarker.style.top = `${posY}px`;
        this.stretchMarker.style.left = `${posX}px`;
    }

    removeMarker() {
        if (!this.stretchMarker) return;

        this.stretchMarker.remove();
        this.stretchMarker = null;
    }
}

extend(Element, {
    semStretchable(
        enable = true,
        horizontalFactor = 1.0,
        verticalFactor = 1.0,
        updatePos = true
    ) {
        const handler = this.remember("_sem_stretchable")
            || new SemStretchableHandler(
                this, horizontalFactor, verticalFactor, updatePos
            );
        handler.init(enable);
        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
        return this;
    },

    isSemStretchable() {
        const handler = this.remember("_sem_stretchable");
        return handler !== undefined && handler.enabled;
    },

    semStretchableConfig() {
        const handler = this.remember("_sem_stretchable");

        if (handler === undefined) {
            return {};
        }

        return {
            horizontalFactor: handler.horizontalFactor,
            verticalFactor: handler.verticalFactor,
            updatePos: handler.updatePos
        };
    },

    updateSemStretchable(
        horizontalFactor = undefined,
        verticalFactor = undefined,
        updatePos = undefined
    ) {
        const handler = this.remember("_sem_stretchable");

        if (!handler) {
            return;
        }

        if (horizontalFactor !== undefined) {
            handler.horizontalFactor = horizontalFactor;
        }

        if (verticalFactor !== undefined) {
            handler.verticalFactor = verticalFactor;
        }

        if (updatePos !== undefined) {
            handler.updatePos = updatePos;
        }

        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
    }
});
