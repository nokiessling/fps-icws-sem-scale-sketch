import Vector2D from "../../math/Vector2D";
import { SvgElementCreated } from "../../events/SvgElementEvents";
import SvgSelectionManager from "../selection/SvgSelectionManager";
import SvgHierarchyManager from "../hierarchy/SvgHierarchyManager";
import ToolManager from "../tools/ToolManager";

export default class SemAnchorer {
    static addAnchoableBehabiour(element) {
        element.semAnchorable();
    }

    static removeAnchorableBehaviour(element) {
        if (!element.isSemAnchorable()) {
            return;
        }

        element.semAnchorable(false);
    }

    static isSemAnchorable(element) {
        return element.isSemAnchorable();
    }

    static getSemAnchorableConfigOfElement(element) {
        return element.semAnchorableConfig();
    }

    static activateAddingOfAnchorPoint(element) {
        if (!element.isSemAnchorable()) {
            return;
        }

        const existingElements = Array.from(
            SvgHierarchyManager.Instance.elementTree.getAllElements()
        );
        const isInteractableSelectionEnabled = SvgSelectionManager.Instance
            .isInteractableSelection;
        const isDraggableSelectionEnabled = SvgSelectionManager.Instance
            .isDraggableSelection;
        const isScalableSelectionEnabled = SvgSelectionManager.Instance
            .isScalableSelection;
        if (isInteractableSelectionEnabled) {
            SvgSelectionManager.Instance
                .deactivateInteractableSelection(existingElements);
        }

        ToolManager.Instance.activate();
        const currentTool = ToolManager.Instance.selectedToolName;
        ToolManager.Instance.selectTool("straightLine");

        const eleRect = element.node.getBoundingClientRect();
        const eleCenter = new Vector2D(
            eleRect.x + eleRect.width / 2,
            eleRect.y + eleRect.height / 2
        );
        ToolManager.Instance.selectedTool.startDrawing(
            eleCenter.x,
            eleCenter.y,
            true,
            true
        );

        const onLineCreatedEvt = evt => {
            evt.stopImmediatePropagation();

            const { endPoint } = ToolManager.Instance.selectedTool;
            const svgEleRect = document.querySelector("svg")
                .getBoundingClientRect();
            const anchorPoint = new Vector2D(
                endPoint[0] + svgEleRect.x,
                endPoint[1] + svgEleRect.y
            );

            element.updateSemAnchorable(
                anchorPoint
            );
            evt.instance.remove();

            ToolManager.Instance.selectTool(currentTool);
            ToolManager.Instance.deactivate();

            if (isInteractableSelectionEnabled) {
                SvgSelectionManager.Instance.activateInteractableSelection(
                    existingElements,
                    isDraggableSelectionEnabled,
                    isScalableSelectionEnabled
                );
            }
        };

        document.addEventListener(
            SvgElementCreated.name,
            onLineCreatedEvt,
            { once: true, capture: true }
        );
    }
}
