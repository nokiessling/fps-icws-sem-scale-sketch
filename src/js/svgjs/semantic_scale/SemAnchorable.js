import {
    Element, extend, SVG, Svg
} from "@svgdotjs/svg.js";
import {
    SvgElementDeleted,
    SvgElementMetadataChanged,
    SvgElementParentChanged
} from "../../events/SvgElementEvents";
import ObserverHandler from "../../helper/ObserverHandler";
import Vector2D from "../../math/Vector2D";
import ListenerHandler from "../../helper/ListenerHandler";

class SemAnchorableHandler {
    constructor(el, anchorPoint) {
        el.remember("_sem_anchorable", this);
        this.enabled = false;
        this.el = el;
        this.xFactorAnchorToParent = null;
        this.yFactorAnchorToParent = null;
        this.xOffsetCentreToAnchor = null;
        this.yOffsetCentreToAnchor = null;
        this.anchorPoint = null;
        if (anchorPoint) {
            this.setNewAnchorPoint(anchorPoint);
        }
        this.semParent = null;
        this.prevParentWidth = null;
        this.prevParentHeight = null;
        this.prevParentPos = null;

        this.elObserverPrevParentWidth = null;
        this.elObserverPrevParentHeight = null;
        this.elObserverPrevParentPos = null;

        this.anchorMarker = null;
        this.anchorLine = null;

        this.elObserverHandler = new ObserverHandler(
            this.el,
            () => {
                if (this.anchorPoint
                && this.elObserverPrevParentPos.x === this.semParent.x()
                && this.elObserverPrevParentPos.y === this.semParent.y()) {
                    if (this.elObserverPrevParentWidth
                        === this.semParent.width()
                    && this.elObserverPrevParentHeight
                        === this.semParent.height()) {
                        this.updateCentreToAnchorOffsets();
                    }
                    if (!this.anchorLine) {
                        this.createAnchorLine();
                    }
                    this.updateAnchorLine();
                }
                this.updateMarker();

                if (this.elObserverPrevParentPos.x !== this.semParent.x()) {
                    this.elObserverPrevParentPos.x = this.semParent.x();
                }
                if (this.elObserverPrevParentPos.y !== this.semParent.y()) {
                    this.elObserverPrevParentPos.y = this.semParent.y();
                }
                if (this.elObserverPrevParentWidth !== this.semParent.width()) {
                    this.elObserverPrevParentWidth = this.semParent.width();
                }
                if (this.elObserverPrevParentHeight
                    !== this.semParent.height()) {
                    this.elObserverPrevParentHeight = this.semParent.height();
                }
            }
        );
        this.parentObserverHandler = null;

        this.listenerHandler = new ListenerHandler(
            this,
            this.el.node,
            "addEventListener",
            "removeEventListener"
        );
    }

    init(enable = true) {
        this.enabled = enable;
        if (enable) {
            if (!this.semParent) {
                this.semParent = this.getSemParent();
            }

            if (this.anchorPoint) {
                this.updateAnchorToParentFactors();
                this.updateCentreToAnchorOffsets();

                this.createAnchorLine();
            }

            this.prevParentWidth = this.semParent.width();
            this.prevParentHeight = this.semParent.height();
            this.prevParentPos = new Vector2D(
                this.semParent.x(), this.semParent.y()
            );
            this.elObserverPrevParentWidth = this.semParent.width();
            this.elObserverPrevParentHeight = this.semParent.height();
            this.elObserverPrevParentPos = new Vector2D(
                this.semParent.x(), this.semParent.y()
            );
            this.addObserverToParent();
            this.listenerHandler.on(SvgElementParentChanged.name, evt => {
                if (evt.instance !== this.el) {
                    return;
                }
                this.onParentChange(evt.newParent);
            });

            this.createMarker();
            this.elObserverHandler.addObserver();
            this.listenerHandler.on(SvgElementDeleted.name, evt => {
                if (evt.instance === this.el) {
                    this.removeAnchorLine();
                    this.removeMarker();
                }
            });
        }
        else {
            this.listenerHandler.removeAllListeners();
            if (this.parentObserverHandler) {
                this.parentObserverHandler.removeObserver();
            }

            this.removeAnchorLine();
            this.removeMarker();
            this.elObserverHandler.removeObserver();
        }
    }

    setNewAnchorPoint(pagePoint) {
        const svgEleNodeBB = document.querySelector("svg")
            .getBoundingClientRect();
        this.anchorPoint = new Vector2D(
            pagePoint.x - svgEleNodeBB.x, // - window.pageXOffset,
            pagePoint.y - svgEleNodeBB.y // - window.pageYOffset
        );

        this.updateAnchorToParentFactors();
        this.updateCentreToAnchorOffsets();

        this.createAnchorLine();
    }

    updateAnchorToParentFactors() {
        if (!this.semParent) {
            this.semParent = this.getSemParent();
        }

        const diffXPosAnchorToParent = this.anchorPoint.x - this.semParent.x();
        const diffYPosAnchorToParent = this.anchorPoint.y - this.semParent.y();

        this.xFactorAnchorToParent = diffXPosAnchorToParent
            / this.semParent.width();
        this.yFactorAnchorToParent = diffYPosAnchorToParent
            / this.semParent.height();
    }

    updateCentreToAnchorOffsets() {
        if (!this.anchorPoint) {
            return;
        }

        this.xOffsetCentreToAnchor = this.el.cx() - this.anchorPoint.x;
        this.yOffsetCentreToAnchor = this.el.cy() - this.anchorPoint.y;
    }

    getSemParent() {
        const parent = this.el.parent();

        if (parent instanceof Svg && parent.isRoot()) {
            return parent;
        }

        const parentOfParent = parent.parent();
        const parentId = parent.data("children-from-node-id");

        return Array.from(
            parentOfParent.children()
        ).find(
            child => child.data("node-id") === parentId
        );
    }

    addObserverToParent() {
        this.parentObserverHandler = new ObserverHandler(
            this.semParent,
            () => {
                const currParentPosX = this.semParent.x();
                const currParentPosY = this.semParent.y();

                const currParentWidth = this.semParent.width();
                const currParentHeight = this.semParent.height();

                if (currParentPosX !== this.prevParentPos.x
                    || currParentPosY !== this.prevParentPos.y) {
                    this.updateAnchorPos();
                    if (currParentWidth === this.prevParentWidth
                        && currParentHeight === this.prevParentHeight) {
                        this.updateCentreToAnchorOffsets();
                    }
                    this.prevParentPos.x = currParentPosX;
                    this.prevParentPos.y = currParentPosY;
                }

                if (currParentWidth !== this.prevParentWidth
                    || currParentHeight !== this.prevParentHeight) {
                    this.updateAnchorPos();
                    this.updatePosAccordingToAnchor();

                    this.prevParentWidth = currParentWidth;
                    this.prevParentHeight = currParentHeight;
                }
            }
        );
        this.parentObserverHandler.addObserver();
    }

    updateAnchorPos() {
        if (!this.anchorPoint) {
            return;
        }

        this.anchorPoint.x = this.semParent.x()
            + this.xFactorAnchorToParent * this.semParent.width();
        this.anchorPoint.y = this.semParent.y()
            + this.yFactorAnchorToParent * this.semParent.height();

        this.updateAnchorLine();
    }

    updatePosAccordingToAnchor() {
        const newCXPos = this.anchorPoint.x + this.xOffsetCentreToAnchor;
        const newCYPos = this.anchorPoint.y + this.yOffsetCentreToAnchor;
        this.el.center(newCXPos, newCYPos);
    }

    onParentChange(newParent) {
        // remove listeners from old parent
        if (this.parentObserverHandler) {
            this.parentObserverHandler.removeObserver();
        }
        this.semParent = newParent;
        if (this.anchorPoint) {
            this.updateAnchorToParentFactors();
            this.updateCentreToAnchorOffsets();
        }

        this.prevParentWidth = this.semParent.width();
        this.prevParentHeight = this.semParent.height();
        this.prevParentPos = new Vector2D(
            this.semParent.x(), this.semParent.y()
        );
        this.elObserverPrevParentWidth = this.semParent.width();
        this.elObserverPrevParentHeight = this.semParent.height();
        this.elObserverPrevParentPos = new Vector2D(
            this.semParent.x(), this.semParent.y()
        );
        this.addObserverToParent();
    }

    createMarker() {
        if (this.anchorMarker) {
            this.removeMarker();
        }

        this.anchorMarker = document.createElement("div");
        this.anchorMarker.style.position = "absolute";
        this.anchorMarker.style.pointerEvents = "none";
        this.anchorMarker.innerHTML = `
                <span class="fas fa-anchor" style='color: lightgrey'></span>
            `;
        document.querySelector("#sem-behaviour-marker")
            .appendChild(this.anchorMarker);
        this.updateMarker();
    }

    createAnchorLine() {
        if (!this.anchorPoint) {
            return;
        }
        if (this.anchorLine) {
            this.removeAnchorLine();
        }

        const svgEle = SVG("svg");
        this.anchorLine = svgEle
            .line([
                [this.el.cx(), this.el.cy()],
                [
                    this.anchorPoint.x,
                    this.anchorPoint.y
                ]
            ]);
        this.anchorLine.fill("none");
        this.anchorLine.stroke({
            color: "#999",
            width: 2,
            linecap: "round",
            linejoin: "round",
            dasharray: "10,10"
        });
        this.anchorLine.marker("end", 3, 3, add => {
            add.circle(3).fill("#999");
        });
        this.anchorLine.node.classList.add("not-for-export");
    }

    updateMarker() {
        if (!this.anchorMarker) {
            return;
        }

        const eleRect = this.el.node.getBoundingClientRect();
        const markerRect = this.anchorMarker.getBoundingClientRect();

        const posX = eleRect.x + eleRect.width - 2 * markerRect.width
                + window.pageXOffset;
        const posY = eleRect.y - markerRect.height - 3 + window.pageYOffset;

        this.anchorMarker.style.top = `${posY}px`;
        this.anchorMarker.style.left = `${posX}px`;
    }

    updateAnchorLine() {
        if (!this.anchorLine) return;

        this.anchorLine.plot([
            [this.el.cx(), this.el.cy()],
            [
                this.anchorPoint.x,
                this.anchorPoint.y
            ]
        ]);
    }

    removeMarker() {
        if (!this.anchorMarker) return;

        this.anchorMarker.remove();
        this.anchorMarker = null;
    }

    removeAnchorLine() {
        if (!this.anchorLine) return;

        this.anchorLine.remove();
        this.anchorLine = null;
    }
}

extend(Element, {
    semAnchorable(
        enable = true,
        anchorPoint = null
    ) {
        const handler = this.remember("_sem_anchorable")
            || new SemAnchorableHandler(this, anchorPoint);
        handler.init(enable);
        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
        return this;
    },

    isSemAnchorable() {
        const handler = this.remember("_sem_anchorable");
        return handler !== undefined && handler.enabled;
    },

    semAnchorableConfig() {
        const handler = this.remember("_sem_anchorable");

        if (handler === undefined) {
            return {};
        }

        return {
            anchorPoint: handler.anchorPoint,
        };
    },

    updateSemAnchorable(
        anchorPoint = undefined
    ) {
        const handler = this.remember("_sem_anchorable");

        if (!handler) {
            return;
        }

        if (anchorPoint !== undefined) {
            handler.setNewAnchorPoint(anchorPoint);
        }

        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
    }
});
