export default class SemStretcher {
    static addStretchableBehaviour(element) {
        element.semStretchable();
    }

    static removeStretchableBehaviour(element) {
        if (!element.isSemStretchable()) {
            return;
        }

        element.semStretchable(false);
    }

    static isSemStretchable(element) {
        return element.isSemStretchable();
    }

    static getSemStretchableConfigOfElement(element) {
        return element.semStretchableConfig();
    }

    static updateHorizontalFactor(element, newHorizontalFactor) {
        if (!element.isSemStretchable()) {
            return;
        }

        element.updateSemStretchable(
            newHorizontalFactor
        );
    }

    static updateVerticalFactor(element, newVerticalFactor) {
        if (!element.isSemStretchable()) {
            return;
        }

        element.updateSemStretchable(
            undefined,
            newVerticalFactor
        );
    }

    static updateUpdatePos(element, shouldUpdatePos) {
        if (!element.isSemStretchable()) {
            return;
        }

        element.updateSemStretchable(
            undefined,
            undefined,
            shouldUpdatePos
        );
    }
}
