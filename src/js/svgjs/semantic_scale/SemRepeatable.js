import {
    Element, extend, Svg, Box,
} from "@svgdotjs/svg.js";
import Vector2D from "../../math/Vector2D";
import {
    SvgElementDeleted,
    SvgElementMetadataChanged,
    SvgElementParentChanged
} from "../../events/SvgElementEvents";
import ObserverHandler from "../../helper/ObserverHandler";
import ListenerHandler from "../../helper/ListenerHandler";

class SemRepeatableHandler {
    constructor(
        el,
        directions,
        repeatSymmetrically,
        path,
        rotateAlongPath,
        amount,
        margin,
        testIntersections
    ) {
        el.remember("_sem_repeatable", this);
        this.enabled = false;
        this.el = el;
        this.directions = directions;
        this.repeatSymmetrically = repeatSymmetrically;
        this.path = path;
        this.rotateAlongPath = rotateAlongPath;
        this.amount = amount;
        this.margin = margin;
        this.testIntersections = testIntersections;
        this.clones = [];
        this.currentCoorSysRot = 0;
        this.repeatMarker = null;
        this.observerHandler = new ObserverHandler(
            this.el,
            () => {
                this.repeat();
                this.updateMarker();
            }
        );
        this.semParent = null;
        this.prevParentWidth = null;
        this.prevParentHeight = null;
        this.parentObserverHandler = null;

        this.listenerHandler = new ListenerHandler(
            this,
            this.el.node,
            "addEventListener",
            "removeEventListener"
        );
    }

    init(enable = true) {
        this.enabled = enable;
        if (enable) {
            this.repeat();
            this.createMarker();
            this.observerHandler.addObserver();

            this.semParent = this.getSemParent();
            this.prevParentWidth = this.semParent.width();
            this.prevParentHeight = this.semParent.height();
            this.addObserverToParent();
            this.listenerHandler.on(SvgElementParentChanged.name, evt => {
                if (evt.instance !== this.el) {
                    return;
                }
                this.onParentChange(evt.newParent);
            });
            this.listenerHandler.on(SvgElementDeleted.name, evt => {
                if (evt.instance === this.el) {
                    this.removeMarker();
                    if (this.path) {
                        this.path.remove();
                    }
                }
            });
        }
        else {
            this.listenerHandler.removeAllListeners();
            this.removeClones();
            this.removeMarker();
            if (this.path) {
                this.path.remove();
            }
            this.observerHandler.removeObserver();

            if (this.parentObserverHandler) {
                this.parentObserverHandler.removeObserver();
            }
        }
    }

    addObserverToParent() {
        this.parentObserverHandler = new ObserverHandler(
            this.semParent,
            () => {
                const currParentWidth = this.semParent.width();
                const currParentHeight = this.semParent.height();

                // only call repeat if change of parent was parents scale
                if (currParentWidth === this.prevParentWidth
                    && currParentHeight === this.prevParentHeight) {
                    return;
                }

                this.repeat();

                this.prevParentWidth = currParentWidth;
                this.prevParentHeight = currParentHeight;
            }
        );
        this.parentObserverHandler.addObserver();
    }

    getSemParent() {
        const parent = this.el.parent();

        if (parent instanceof Svg && parent.isRoot()) {
            return parent;
        }

        const parentOfParent = parent.parent();
        const parentId = parent.data("children-from-node-id");

        return Array.from(
            parentOfParent.children()
        ).find(
            child => child.data("node-id") === parentId
        );
    }

    getParentBBox() {
        const semParent = this.getSemParent();

        if (semParent instanceof Svg && semParent.isRoot()) {
            const { clientWidth, clientHeight } = semParent.node;
            return new Box(0, 0, clientWidth, clientHeight);
        }

        return semParent.bbox();
    }

    onParentChange(newParent) {
        // remove listeners from old parent
        if (this.parentObserverHandler) {
            this.parentObserverHandler.removeObserver();
        }
        this.semParent = newParent;
        this.prevParentWidth = this.semParent.width();
        this.prevParentHeight = this.semParent.height();
        this.addObserverToParent();
    }

    repeat() {
        this.removeClones();

        if (this.path) {
            this.buildClonesAlongPath(this.path, this.rotateAlongPath);
            return;
        }

        const gridDirIndex = this.directions.findIndex(
            value => value.x === 0 && value.y === 0
        );
        // repeat in grid
        if (gridDirIndex > -1) {
            const right = new Vector2D(1, 0);
            const left = new Vector2D(-1, 0);
            const up = new Vector2D(0, -1);
            const down = new Vector2D(0, 1);

            const amountRight = this.calcAmountInDirection(right);
            const amountLeft = this.calcAmountInDirection(left);
            const amountUp = this.calcAmountInDirection(up);
            const amountDown = this.calcAmountInDirection(down);

            if (this.repeatSymmetrically) {
                this.buildCloneGrid(
                    amountRight,
                    amountRight,
                    amountUp,
                    amountUp
                );
                return;
            }
            this.buildCloneGrid(amountRight, amountLeft, amountUp, amountDown);
            return;
        }

        for (let i = 0; i < this.directions.length; i++) {
            const direction = this.directions[i];
            // grid
            if (direction.x === 0 && direction.y === 0) {
                continue;
            }

            let amount = this.calcMaxPossibleAmountInParentInDirection(
                direction
            );
            const dirKeyInAmountMap = this.getKeyOfAmountInDirection(direction);
            if (dirKeyInAmountMap !== undefined
                && this.amount.get(dirKeyInAmountMap) !== -1) {
                amount = Math.min(
                    this.amount.get(dirKeyInAmountMap),
                    amount
                );
            }
            // // diagonal
            // if (direction.x !== 0 && direction.y !== 0) {
            //     amount = Math.min(amountX, amountY);
            // }
            // // x axis
            // else if (direction.x !== 0) {
            //     amount = amountX;
            // }
            // // y axis
            // else if (direction.y !== 0) {
            //     amount = amountY;
            // }
            this.buildClonesInDirection(amount, direction.x, direction.y);
        }
    }

    getKeyOfAmountInDirection(direction) {
        return Array.from(this.amount.keys())
            .find(vec => vec.x === direction.x && vec.y === direction.y);
    }

    calcMaxPossibleAmountInParentInDirection(direction) {
        // get bbox of parent to repeat in parent
        // (if there is no other sketch element, this would be the canvas)
        const bbParent = this.getParentBBox();
        const bb = this.el.bbox();

        let amtX = 0;
        if (direction.x > 0) {
            amtX = Math.floor(
                (bbParent.width - (bb.x + bb.width - bbParent.x))
                / (bb.width + this.margin)
            );
        }
        else if (direction.x < 0) {
            amtX = Math.floor(
                (bb.x - bbParent.x) / (bb.width + this.margin)
            );
        }

        let amtY = 0;
        if (direction.y > 0) {
            amtY = Math.floor(
                (bbParent.height - (bb.y + bb.height - bbParent.y))
                / (bb.height + this.margin)
            );
        }
        else if (direction.y < 0) {
            amtY = Math.floor(
                (bb.y - bbParent.y)
                / (bb.height + this.margin)
            );
        }

        if (direction.x === 0 && direction.y !== 0) {
            return amtY;
        }
        if (direction.x !== 0 && direction.y === 0) {
            return amtX;
        }
        return Math.min(amtX, amtY);
    }

    calcAmountInDirection(direction) {
        const maxAmount = this
            .calcMaxPossibleAmountInParentInDirection(direction);
        const keyOfDirAmount = this.getKeyOfAmountInDirection(direction);

        if (this.amount.get(keyOfDirAmount) === -1) {
            return maxAmount;
        }

        return Math.min(this.amount.get(keyOfDirAmount), maxAmount);
    }

    buildClonesInDirection(amount, x, y) {
        const bb = this.el.bbox();
        for (let i = 0; i < amount; i++) {
            this.buildClone(
                x * (bb.width + this.margin) * (i + 1),
                y * (bb.height + this.margin) * (i + 1)
            );
        }
    }

    buildCloneGrid(amountRight, amountLeft, amountUp, amountDown) {
        this.buildCloneGridQuarterInDir(
            amountRight,
            amountDown,
            new Vector2D(1, 1)
        );
        this.buildCloneGridQuarterInDir(
            amountRight,
            amountUp,
            new Vector2D(1, -1)
        );
        this.buildCloneGridQuarterInDir(
            amountLeft,
            amountDown,
            new Vector2D(-1, 1)
        );
        this.buildCloneGridQuarterInDir(
            amountLeft,
            amountUp,
            new Vector2D(-1, -1)
        );
    }

    buildCloneGridQuarterInDir(amountX, amountY, direction) {
        const leaveOutFirstCol = direction.x < 0;
        const leaveOutFirstRow = direction.y < 0;
        const bb = this.el.bbox();
        const cols = amountX + 1;
        const rows = amountY + 1;
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                if (i === 0 && j === 0) {
                    continue;
                }
                if (leaveOutFirstCol && i === 0) {
                    continue;
                }
                if (leaveOutFirstRow && j === 0) {
                    continue;
                }
                this.buildClone(
                    direction.x * i * (bb.width + this.margin),
                    direction.y * j * (bb.height + this.margin)
                );
            }
        }
    }

    buildClonesAlongPath(path, rotate = false) {
        // reset coordinate system to prevent weird offsets
        // and rotation on top of old rotation
        // this.el.rotate((-1) * this.currentCoorSysRot);

        const length = path.length();
        const bb = this.el.bbox();
        const elSpace = bb.width + this.margin;
        // TODO: use given amount if it exists
        const maxAmount = Math.floor(length / elSpace);
        let currLength = 0;
        let pointOnPathAtLength = path.pointAt(currLength);
        const originalPosX = pointOnPathAtLength.x;
        const originalPosY = pointOnPathAtLength.y;
        if (this.el.cx() !== originalPosX || this.el.cy() !== originalPosY) {
            this.el.center(originalPosX, originalPosY);
        }

        let originalRotAngle = 0;
        if (rotate) {
            const secondPoint = path.pointAt(currLength + elSpace);
            originalRotAngle = this.calcGradientAngleBetweenPoints(
                pointOnPathAtLength, secondPoint
            );
            originalRotAngle = 90 - originalRotAngle;
        }
        this.currentCoorSysRot = originalRotAngle;
        const resetRotAngle = (-1) * this.currentCoorSysRot;
        if ((resetRotAngle + originalRotAngle) !== 0) {
            // reset coordinate system to prevent weird offsets
            // and rotation on top of old rotation
            this.el.rotate(resetRotAngle);
            this.el.rotate(originalRotAngle);
        }

        let diffX;
        let diffY;
        for (let i = 0; i < maxAmount; i++) {
            currLength += elSpace;
            pointOnPathAtLength = path.pointAt(currLength);
            diffX = pointOnPathAtLength.x - originalPosX;
            diffY = pointOnPathAtLength.y - originalPosY;

            let rotAngle = 0;
            if (rotate) {
                const secondPoint = path.pointAt(currLength - elSpace);
                rotAngle = this.calcGradientAngleBetweenPoints(
                    secondPoint, pointOnPathAtLength
                );
                rotAngle = 90 - rotAngle;
            }

            this.buildClone(diffX, diffY, rotAngle - originalRotAngle);
        }
    }

    calcGradientAngleBetweenPoints(point1, point2) {
        const deltaX = point2.x - point1.x;
        const deltaY = point1.y - point2.y;
        return Math.atan(deltaY / deltaX) * 180 / Math.PI;
    }

    buildClone(posX, posY, rotAngle = 0) {
        // don't create clone if it would not be in parent
        // doesn't always work
        if (this.testIntersections
            && (
                !this.isPointInsideParentBB(
                    this.el.x() + posX,
                    this.el.y() + posY
                )
                || !this.isPointInsideParentBB(
                    this.el.x() + posX + this.el.width(),
                    this.el.y() + posY + this.el.height()
                )
            )) {
            return;
        }

        this.clones.push(
            this.el.root()
                .use(this.el)
                .move(posX, posY)
                .rotate(rotAngle)
        );
    }

    isPointInsideParentBB(x, y) {
        const parentBB = this.getParentBBox();
        return x >= parentBB.x && x <= parentBB.x + parentBB.width
        && y >= parentBB.y && y <= parentBB.y + parentBB.height;
    }

    removeClones() {
        this.clones.map(c => c.remove());
        this.clones = [];
    }

    createMarker() {
        if (this.repeatMarker) {
            this.removeMarker();
        }

        this.repeatMarker = document.createElement("div");
        this.repeatMarker.style.position = "absolute";
        this.repeatMarker.style.pointerEvents = "none";
        this.repeatMarker.innerHTML = `
                <span class="fas fa-clone" style='color: lightgrey'></span>
            `;
        document.querySelector("#sem-behaviour-marker")
            .appendChild(this.repeatMarker);
        this.updateMarker();
    }

    updateMarker() {
        if (!this.repeatMarker) {
            return;
        }

        const eleRect = this.el.node.getBoundingClientRect();
        const markerRect = this.repeatMarker.getBoundingClientRect();
        const posX = eleRect.x
            + eleRect.width
            - markerRect.width - 3
            + window.pageXOffset;
        const posY = eleRect.y - markerRect.height - 3 + window.pageYOffset;

        this.repeatMarker.style.top = `${posY}px`;
        this.repeatMarker.style.left = `${posX}px`;
    }

    removeMarker() {
        if (!this.repeatMarker) return;

        this.repeatMarker.remove();
        this.repeatMarker = null;
    }
}

extend(Element, {
    semRepeatable(
        enable = true,
        directions = [],
        repeatSymmetrically = true,
        path = null,
        rotateAlongPath = false,
        amount = new Map([
            [new Vector2D(-1, -1), -1],
            [new Vector2D(0, -1), -1],
            [new Vector2D(1, -1), -1],
            [new Vector2D(-1, 0), -1],
            [new Vector2D(1, 0), -1],
            [new Vector2D(-1, 1), -1],
            [new Vector2D(0, 1), -1],
            [new Vector2D(1, 1), -1],
        ]),
        margin = 10,
        testIntersections = false
    ) {
        const handler = this.remember("_sem_repeatable")
            || new SemRepeatableHandler(
                this,
                directions,
                repeatSymmetrically,
                path,
                rotateAlongPath,
                amount,
                margin,
                testIntersections
            );
        handler.init(enable);
        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
        return this;
    },

    isSemRepeatable() {
        const handler = this.remember("_sem_repeatable");
        return handler !== undefined && handler.enabled;
    },

    semRepeatableConfig() {
        const handler = this.remember("_sem_repeatable");

        if (!handler) {
            return {};
        }

        return {
            directions: [...handler.directions],
            repeatSymmetrically: handler.repeatSymmetrically,
            path: handler.path,
            rotateAlongPath: handler.rotateAlongPath,
            amount: handler.amount,
            margin: handler.margin,
            testIntersections: handler.testIntersections
        };
    },

    updateSemRepeatable(
        directions = undefined,
        repeatSymmetrically = undefined,
        path = undefined,
        rotateAlongPath = undefined,
        amount = undefined,
        margin = undefined,
        testIntersections = undefined
    ) {
        const handler = this.remember("_sem_repeatable");

        if (!handler) {
            return;
        }

        if (directions !== undefined) {
            handler.directions = directions;
        }

        if (repeatSymmetrically !== undefined) {
            handler.repeatSymmetrically = repeatSymmetrically;
        }

        if (path !== undefined) {
            if (handler.path) {
                handler.path.remove();
            }
            handler.path = path;
        }

        if (rotateAlongPath !== undefined) {
            handler.rotateAlongPath = rotateAlongPath;
        }

        if (amount !== undefined) {
            handler.amount = amount;
        }

        if (margin !== undefined) {
            handler.margin = margin;
        }

        if (testIntersections !== undefined) {
            handler.testIntersections = testIntersections;
        }

        handler.repeat();

        // handler.el.fire("updated");
        document.dispatchEvent(new SvgElementMetadataChanged(handler.el));
    },
});
