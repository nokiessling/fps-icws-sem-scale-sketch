export default class NumInput extends HTMLElement {
    constructor() {
        super();

        this.innerHTML = `
            <div class="input-group input-group-sm">
                <input type="text" class="form-control value-input-field" style="width: 40px" value="${this.value}" min="${this.min}" max="${this.max}" step="${this.step}">
                <div class="btn-group-vertical">
                    <button type="button" class="btn btn-outline-secondary d-flex align-items-center increase-value-btn" style="height: 20px; font-size: 10px">
                        <span class="fa fa-plus"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary d-flex align-items-center decrease-value-btn" style="height: 20px; font-size: 10px">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        `;

        this.querySelector(".increase-value-btn")
            .addEventListener(
                "click",
                () => {
                    this.value += this.step;
                }
            );

        this.querySelector(".decrease-value-btn")
            .addEventListener(
                "click",
                () => {
                    this.value -= this.step;
                }
            );

        this.querySelector(".value-input-field")
            .addEventListener(
                "change",
                event => {
                    this.value = event.target.value;
                }
            );
    }

    get value() {
        const valueString = this.getAttribute("value");

        if (valueString === null || valueString === "") {
            return -1;
        }

        return parseFloat(valueString);
    }

    set value(val) {
        const currentValue = this.value;
        const newValue = Math.max(this.min, val);

        if (currentValue === newValue) {
            return;
        }

        const valStr = newValue.toString();

        this.setAttribute("value", valStr);
        this.querySelector(".value-input-field").value = valStr;

        this.dispatchEvent(new Event("changed"));
    }

    get step() {
        const stepString = this.getAttribute("step");

        if (stepString === null || stepString === "") {
            return 1;
        }

        return parseFloat(stepString);
    }

    get min() {
        const minString = this.getAttribute("min");

        if (minString === null || minString === "") {
            return -1;
        }

        return parseFloat(minString);
    }

    get max() {
        const maxString = this.getAttribute("max");

        if (maxString === null || maxString === "") {
            return 100;
        }

        return parseFloat(maxString);
    }
}

window.customElements.define("num-input", NumInput);
