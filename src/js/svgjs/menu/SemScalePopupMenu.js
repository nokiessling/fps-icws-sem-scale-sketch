import {
    SvgElementDeleted,
    SvgElementDeselected,
    SvgElementMetadataChanged,
    SvgElementSelected
} from "../../events/SvgElementEvents";
import ListenerHandler from "../../helper/ListenerHandler";
import ObserverHandler from "../../helper/ObserverHandler";
import SemRepeater from "../semantic_scale/SemRepeater";
import SemStretcher from "../semantic_scale/SemStretcher";
import SemAnchorer from "../semantic_scale/SemAnchorer";

export default class SemScalePopupMenu {
    constructor() {
        this.htmlId = "#popup-menu";
        this.htmlPopupToolbarsId = "#popup-menu-toolbars";
        this.htmlPopupMainToolbarId = "#popup-main-toolbar";
        this.htmlPopupSurroundingElementsId = "#popup-menu-surrounding-elements";

        this.htmlSemRepeatBtnId = "#popup-sem-repeat";
        this.htmlSemStretchBtnId = "#popup-sem-stretch";
        this.htmlSemAnchorBtnId = "#popup-sem-anchor";

        this.htmlSemRepeatToolbarId = "#sem-repeat-toolbar";
        this.htmlSemRepeatSurrElemsId = "#popup-surrounding-elements-sem-repeat";

        this.htmlSurrRepeatDirsElemsId = "#repeat-dirs-surr-elems";
        this.htmlRepeatDirsNumInputsId = "#repeat-dirs-num-inputs";
        this.htmlSurrRepeatGridElemsId = "#repeat-grid-surr-elems";
        this.htmlSurrRepeatPathElemsId = "#repeat-path-surr-elems";

        this.htmlRepeatPathRotationToggleId = "#repeat-path-rotation-toggle";

        this.htmlSemStretchSurrElemsId = "#popup-surrounding-elements-sem-stretch";

        this.htmlSemAnchorSurrElemsId = "#popup-surrounding-elements-sem-anchor";
        this.verticalOffset = 25;
        this.selectedElement = null;
        this.selectedSemType = null;
        this.currentRepeatType = null;
        this.selectedRepeatType = null;

        const menu = this.createPopupMenu();
        document.querySelector("#root")
            .appendChild(menu);
        this.initPopupMenu();

        this.listenerHandler = new ListenerHandler(
            this,
            document,
            "addEventListener",
            "removeEventListener"
        );

        this.observerHandler = new ObserverHandler();
    }

    activate(selectedElement = null) {
        this.listenerHandler.on(SvgElementSelected.name, evt => {
            this.showMenu(evt.instance);
        });

        this.listenerHandler.on(SvgElementDeselected.name, evt => {
            this.hideMenu();
        });

        this.listenerHandler.on(SvgElementMetadataChanged.name, evt => {
            this.initMenuWithElementsMetadata(evt.instance);
        });

        this.listenerHandler.on(SvgElementDeleted.name, evt => {
            if (this.selectedElement != null
                && this.selectedElement === evt.instance) {
                this.hideMenu();
            }
        });

        if (selectedElement) {
            this.showMenu(selectedElement);
        }
    }

    deactivate() {
        this.listenerHandler.removeAllListeners();

        this.hideMenu();
    }

    createPopupMenu() {
        const div = document.createElement("div");
        div.id = "popup-menu";
        div.style.zIndex = 1;
        div.classList.add("d-none");
        div.style.pointerEvents = "none";

        div.appendChild(this.createToolbars());
        div.appendChild(this.createSurroundingElements());

        return div;
    }

    createToolbars() {
        const div = document.createElement("div");
        div.id = "popup-menu-toolbars";
        // div.style.zIndex = 1;
        div.style.position = "absolute";

        div.innerHTML = `
            <div id="popup-main-toolbar" class="btn-toolbar mb-1" style="pointer-events: auto">
                <div class="btn-group" role="group">
                    <button id="popup-sem-repeat" type="button" class="btn btn-secondary" data-sem-type="repeat"
                            data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-trigger="hover" title="Set semantic repeat behaviour">
                        <span class="fas fa-table"></span>
                    </button>
                    <button id="popup-sem-stretch" type="button" class="btn btn-secondary" data-sem-type="stretch" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Set semantic stretch behaviour">
                        <span class="fas fa-compress"></span>
                    </button>
                    <button id="popup-sem-anchor" type="button" class="btn btn-secondary" data-sem-type="anchor" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Mark semantic anchor">
                        <span class="fas fa-anchor"></span>
                    </button>
                </div>
            </div>
            <div id="sem-repeat-toolbar" class="btn-toolbar d-none" style="pointer-events: auto">
                <div class="btn-group btn-group-sm" role="group">
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="directions"
                            data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-trigger="hover" title="Repeat in defined directions">
                        <span class="fas fa-arrows-alt"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="grid" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Repeat in grid">
                        <span class="fas fa-th"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="direction" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Define direction to repeat in">
                        <span class="fas fa-long-arrow-alt-right"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="path"
                            data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-trigger="hover" title="Define path to repeat along">
                        <span class="fas fa-wave-square"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="at_element" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Select element to repeat at" disabled>
                        <span class="fas fa-vector-square"></span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary" data-repeat-type="at_line" data-bs-toggle="tooltip"
                            data-bs-placement="bottom" data-bs-trigger="hover" title="Mark line to repeat along" disabled>
                        <span class="fas fa-highlighter"></span>
                    </button>
                </div>
            </div>
        `;

        return div;
    }

    createSurroundingElements() {
        const div = document.createElement("div");
        div.id = "popup-menu-surrounding-elements";
        div.style.position = "absolute";
        div.classList.add("d-none");

        const createElement = (
            dimOne,
            dimOneValue,
            dimTwo,
            dimTwoValue,
            innerElement
        ) => `
            <div style="position: absolute; ${dimOne}: ${dimOneValue}%; ${dimTwo}: ${dimTwoValue}%; transform: translate(-${Math.max(dimTwoValue, 0)}%, -${Math.max(dimOneValue, 0)}%); pointer-events: auto">
                ${innerElement}
            </div>
        `;

        const createButton = (
            idText,
            customClass,
            tooltipText,
            icon,
            iconRotDegree,
            customDataAttr,
            customDataValue
        ) => `
                <button id=${idText} type="button" class="btn btn-outline-secondary ${customClass}"
                        ${customDataAttr}="${customDataValue}"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" 
                        data-bs-trigger="hover" title="${tooltipText}">
                    <span style="transform: rotate(${iconRotDegree}deg)" class="fas ${icon}"></span>
                </button>
        `;

        div.innerHTML = `
            <div id="popup-surrounding-elements-sem-repeat" class="popup-surr-elements-of-sem-type" data-sem-type="repeat" style="position: relative; width: 100%; height: 100%">
                <div id="repeat-dirs-surr-elems" class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="directions" data-overflow-width="100" data-overflow-height="100">
                    <div id="repeat-dirs-num-inputs" class="d-none" style="position: relative; width: 100%; height: 100%">
                        ${createElement("top", -50, "left", -50, `
                            <num-input class="d-none" data-repeat-direction="top_left"></num-input>
                        `)}
                        ${createElement("top", -50, "left", 50, `
                            <num-input class="d-none" data-repeat-direction="top"></num-input>
                        `)}
                        ${createElement("top", -50, "right", -50, `
                            <num-input class="d-none" data-repeat-direction="top_right"></num-input>
                        `)}
                        ${createElement("top", 50, "left", -50, `
                            <num-input class="d-none" data-repeat-direction="left"></num-input>
                        `)}
                        ${createElement("top", 50, "right", -50, `
                            <num-input class="d-none" data-repeat-direction="right"></num-input>
                        `)}
                        ${createElement("bottom", -50, "left", -50, `
                            <num-input class="d-none" data-repeat-direction="bottom_left"></num-input>
                        `)}
                        ${createElement("bottom", -50, "left", 50, `
                            <num-input class="d-none" data-repeat-direction="bottom"></num-input>
                        `)}
                        ${createElement("bottom", -50, "right", -50, `
                            <num-input class="d-none" data-repeat-direction="bottom_right"></num-input>
                        `)}
                    </div>                    
                    ${createElement("top", 0, "left", 0, `
                        ${createButton("repeat-dir-top-left", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-up", -45, "data-repeat-direction", "top_left")}
                    `)}
                    ${createElement("top", 0, "left", 50, `
                        ${createButton("repeat-dir-top", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-up", 0, "data-repeat-direction", "top")}
                    `)}
                    ${createElement("top", 0, "right", 0, `
                        ${createButton("repeat-dir-top-right", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-up", 45, "data-repeat-direction", "top_right")}
                    `)}
                    ${createElement("top", 50, "left", 0, `
                        ${createButton("repeat-dir-left", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-left", 0, "data-repeat-direction", "left")}
                    `)}
                    ${createElement("top", 50, "right", 0, `
                        ${createButton("repeat-dir-right", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-right", 0, "data-repeat-direction", "right")}
                    `)}
                    ${createElement("bottom", 0, "left", 0, `
                        ${createButton("repeat-dir-bottom-left", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-down", 45, "data-repeat-direction", "bottom_left")}
                    `)}
                    ${createElement("bottom", 0, "left", 50, `
                        ${createButton("repeat-dir-bottom", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-down", 0, "data-repeat-direction", "bottom")}
                    `)}
                    ${createElement("bottom", 0, "right", 0, `
                        ${createButton("repeat-dir-bottom-right", "repeat-direction-btn", "repeat element in this direction", "fa-long-arrow-alt-down", -45, "data-repeat-direction", "bottom_right")}
                    `)}
                </div>
                <div id="repeat-grid-surr-elems" class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="grid" data-overflow-width="190" data-overflow-height="150">
                    ${createElement("top", 0, "left", 50, `
    <!--                    <span class="fa fa-arrows-alt-v"></span>-->
                        <num-input id="num-input-grid-top" data-repeat-direction="top"></num-input>
                    `)}
                    ${createElement("top", 0, "right", 0, `
                        ${createButton("repeat-grid-symmetry-toggle", "", "toggle symmetric repeat", "fa-link", 0, "data-repeat-symmetrically", true)}
                    `)}
                    ${createElement("top", 50, "left", 0, `
                        <num-input id="num-input-grid-left" class="d-none asymmetric-repeat-amount" data-repeat-direction="left"></num-input>
                    `)}
                    ${createElement("top", 50, "right", 0, `
                        <num-input id="num-input-grid-right" data-repeat-direction="right"></num-input>    
    <!--                    <span class="fa fa-arrows-alt-h" style="width: 100%"></span>-->
                    `)} 
                    ${createElement("bottom", 0, "left", 50, `
                        <num-input id="num-input-grid-bottom" class="d-none asymmetric-repeat-amount" data-repeat-direction="bottom"></num-input>
                    `)}
                </div>
                <div id="repeat-direction-surr-elems" class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="direction" data-overflow-width="110" data-overflow-height="110">
                </div>
                <div id="repeat-path-surr-elems" class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="path" data-overflow-width="190" data-overflow-height="150">
                    ${createElement("top", 0, "right", 0, `
                        ${createButton("repeat-path-rotation-toggle", "", "toggle rotation along path", "fa-sync-alt", 0, "data-rotate-along-path", false)}
                    `)}
                </div>
                <div class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="at_element" data-overflow-width="190" data-overflow-height="150">
                </div>
                <div class="repeat-surr-elems d-none" style="position: relative; width: 100%; height: 100%" data-repeat-type="at_line" data-overflow-width="190" data-overflow-height="150">
                </div>
            </div>
            <div id="popup-surrounding-elements-sem-stretch" class="popup-surr-elements-of-sem-type" data-sem-type="stretch" style="position: relative; width: 100%; height: 100%" data-overflow-width="70" data-overflow-height="70">
                ${createElement("top", 50, "right", -50, `
                    <span class="fas fa-arrows-alt-v" style="width: 20px; height: 40px"></span>
                    <num-input id="stretch-vertical-num-input" value="1" min="-100" max="100" step="0.5"></num-input>
                `)}
                ${createElement("bottom", -50, "left", 50, `
                    <span class="fas fa-arrows-alt-h"></span>
                    <num-input id="stretch-horizontal-num-input" value="1" min="-100" max="100" step="0.5"></num-input>
                `)}
            </div>
            <div id="popup-surrounding-elements-sem-anchor" class="popup-surr-elements-of-sem-type" data-sem-type="anchor" style="position: relative; width: 100%; height: 100%" data-overflow-width="100" data-overflow-height="100">
                ${createElement("top", 0, "right", 0, `
                    ${createButton("anchor-edit-btn", "", "Define new anchor point position", "fa-edit", 0, "", "")}
                `)}
            </div>
        `;

        const stretchVerticalDiv = div
            .querySelector("#stretch-vertical-num-input")
            .parentElement;
        stretchVerticalDiv.classList.add("d-flex");
        stretchVerticalDiv.classList.add("justify-content-between");

        return div;
    }

    initPopupMenu() {
        this.initToolbars();
        this.initSurroundingElements();
    }

    initToolbars() {
        // main toolbar
        const mainToolbar = document.querySelector(this.htmlPopupMainToolbarId);
        const semTypeButtons = mainToolbar.querySelectorAll("button");
        semTypeButtons.forEach(btn => {
            btn.addEventListener("click", evt => {
                this.selectSemType(btn.dataset.semType);
            });
        });

        // semantic repeat toolbar
        const repeatToolbar = document
            .querySelector(this.htmlSemRepeatToolbarId);
        const repeatToolbarBtns = repeatToolbar.querySelectorAll("button");
        repeatToolbarBtns.forEach(btn => btn.addEventListener("click", evt => {
            this.onRepeatTypeChanged(btn.dataset.repeatType);
        }));
    }

    initSurroundingElements() {
        const dirBtns = document.querySelector(this.htmlSurrRepeatDirsElemsId)
            .querySelectorAll(".repeat-direction-btn");
        dirBtns.forEach(btn => btn.addEventListener("click", evt => {
            // this.onSemRepeatDirectionButtonClick(btn);
            SemRepeater.toggleRepeatDir(
                this.selectedElement,
                btn.dataset.repeatDirection
            );
        }));

        const dirsNumInputs = document
            .querySelector(this.htmlRepeatDirsNumInputsId)
            .querySelectorAll("num-input");
        dirsNumInputs.forEach(numInput => {
            numInput.addEventListener("changed", event => {
                SemRepeater.updateAmountOfDir(
                    this.selectedElement,
                    SemRepeater.vectorMap.get(numInput.dataset.repeatDirection),
                    Number.parseInt(numInput.value, 10)
                );
            });
        });

        const repeatGridSurrElemsDiv = document
            .querySelector(this.htmlSurrRepeatGridElemsId);
        const gridSymmetryToggle = repeatGridSurrElemsDiv
            .querySelector("#repeat-grid-symmetry-toggle");
        gridSymmetryToggle.classList
            .replace("btn-outline-secondary", "btn-outline-primary");

        const asymmetricRepeatInputFields = repeatGridSurrElemsDiv
            .querySelectorAll(".asymmetric-repeat-amount");
        gridSymmetryToggle.addEventListener("click", evt => {
            const repeatSymmetrically = gridSymmetryToggle
                .dataset
                .repeatSymmetrically === "false";
            if (repeatSymmetrically) {
                gridSymmetryToggle.classList
                    .replace("btn-outline-secondary", "btn-outline-primary");
                asymmetricRepeatInputFields
                    .forEach(field => field.classList.add("d-none"));
                gridSymmetryToggle.dataset.repeatSymmetrically = "true";
            }
            else {
                gridSymmetryToggle.classList
                    .replace("btn-outline-primary", "btn-outline-secondary");
                asymmetricRepeatInputFields
                    .forEach(field => field.classList.remove("d-none"));
                gridSymmetryToggle.dataset.repeatSymmetrically = "false";
            }
            SemRepeater.updateRepeatSymmetrically(
                this.selectedElement,
                repeatSymmetrically
            );
        });

        const gridNumInputs = repeatGridSurrElemsDiv
            .querySelectorAll("num-input");
        gridNumInputs.forEach(numInput => {
            numInput.addEventListener("changed", evt => {
                SemRepeater.updateAmountOfDir(
                    this.selectedElement,
                    SemRepeater.vectorMap.get(numInput.dataset.repeatDirection),
                    Number.parseInt(numInput.value, 10)
                );
            });
        });

        const repeatPathRotationToggle = document
            .querySelector(this.htmlRepeatPathRotationToggleId);
        repeatPathRotationToggle.addEventListener("click", evt => {
            const rotate = repeatPathRotationToggle
                .dataset
                .rotateAlongPath === "false";
            if (rotate) {
                repeatPathRotationToggle.classList.replace(
                    "btn-outline-secondary",
                    "btn-outline-primary"
                );
                repeatPathRotationToggle.dataset.rotateAlongPath = "true";
            }
            else {
                repeatPathRotationToggle.classList.replace(
                    "btn-outline-primary",
                    "btn-outline-secondary"
                );
                repeatPathRotationToggle.dataset.rotateAlongPath = "false";
            }
            SemRepeater.updateRotateAlongPath(this.selectedElement, rotate);
        });

        const stretchSurrElemsDiv = document
            .querySelector(this.htmlPopupSurroundingElementsId)
            .querySelector("[data-sem-type=\"stretch\"]");
        const stretchHorizontalFactorNumInput = stretchSurrElemsDiv
            .querySelector("#stretch-horizontal-num-input");
        stretchHorizontalFactorNumInput.addEventListener("click", evt => {
            SemStretcher.updateHorizontalFactor(
                this.selectedElement,
                Number.parseFloat(stretchHorizontalFactorNumInput.value)
            );
        });
        const stretchVerticalFactorNumInput = stretchSurrElemsDiv
            .querySelector("#stretch-vertical-num-input");
        stretchVerticalFactorNumInput.addEventListener("click", evt => {
            SemStretcher.updateVerticalFactor(
                this.selectedElement,
                Number.parseFloat(stretchVerticalFactorNumInput.value)
            );
        });

        const anchorEditBtn = document
            .querySelector(this.htmlPopupSurroundingElementsId)
            .querySelector(this.htmlSemAnchorSurrElemsId)
            .querySelector("#anchor-edit-btn");
        anchorEditBtn.addEventListener("click", evt => {
            SemAnchorer.activateAddingOfAnchorPoint(this.selectedElement);
        });
    }

    showMenu(selectedElement) {
        if (this.selectedElement === selectedElement) {
            return;
        }
        if (this.selectedElement) {
            this.observerHandler.removeObserver();
            this.selectedSemType = null;
            this.currentRepeatType = null;
            this.selectedRepeatType = null;
        }
        this.selectedElement = selectedElement;

        document.querySelector(this.htmlId)
            .classList.remove("d-none");
        this.initMenuWithElementsMetadata(selectedElement);
        this.updatePositions();

        this.observerHandler.addObserver(
            this.selectedElement, () => this.updatePositions()
        );
    }

    hideMenu() {
        document.querySelector(this.htmlId)
            .classList.add("d-none");
        this.observerHandler.removeObserver();
        this.selectedElement = null;
        this.selectedSemType = null;
        this.currentRepeatType = null;
        this.selectedRepeatType = null;
    }

    initMenuWithElementsMetadata(selectedElement) {
        const isRepeatable = SemRepeater.isSemRepeatable(selectedElement);
        const semRepeatToolbar = document
            .querySelector(this.htmlSemRepeatToolbarId);
        const surrElemsDiv = document
            .querySelector(this.htmlPopupSurroundingElementsId);
        const semRepeatSurrElems = document
            .querySelector(this.htmlSemRepeatSurrElemsId);

        if (isRepeatable
            && (!this.selectedSemType || this.selectedSemType === "repeat")) {
            if (!this.selectedSemType) {
                this.selectedSemType = "repeat";
            }

            semRepeatToolbar.classList.remove("d-none");
            surrElemsDiv.classList.remove("d-none");
            semRepeatSurrElems.classList.remove("d-none");

            const {
                directions,
                path
            } = SemRepeater.getSemRepeatableConfigOfElement(selectedElement);

            this.currentRepeatType = null;
            if (path) {
                this.currentRepeatType = "path";
            }
            else if (directions !== undefined && directions
                .find(value => value.x === 0 && value.y === 0) !== undefined) {
                this.currentRepeatType = "grid";
            }
            else if (directions !== undefined) {
                const customDirs = directions
                    .filter(value => value.x > -1 && value.x < 0
                        || value.x > 0 && value.x < 1
                        || value.y > -1 && value.y < 0
                        || value.y > 0 && value.y < 1);
                if (this.selectedRepeatType !== null
                    && (this.selectedRepeatType === "direction"
                        || this.selectedRepeatType === "directions")) {
                    this.currentRepeatType = this.selectedRepeatType;
                }
                else if (customDirs !== undefined && customDirs.length > 0) {
                    this.currentRepeatType = "direction";
                }
                else if (customDirs === undefined
                    || customDirs.length !== directions.length) {
                    this.currentRepeatType = "directions";
                }
            }

            semRepeatToolbar.querySelectorAll("button")
                .forEach(btn => {
                    if (this.currentRepeatType === btn.dataset.repeatType) {
                        btn.classList.replace(
                            "btn-outline-secondary",
                            "btn-outline-primary"
                        );
                    }
                    else {
                        btn.classList.replace(
                            "btn-outline-primary",
                            "btn-outline-secondary"
                        );
                    }
                });
            semRepeatSurrElems.querySelectorAll(".repeat-surr-elems")
                .forEach(div => {
                    if (div.dataset.repeatType === this.currentRepeatType) {
                        div.classList.remove("d-none");
                    }
                    else {
                        div.classList.add("d-none");
                    }
                });

            this.initSurrElemsOfRepeatTypeWithMetadata(
                selectedElement,
                this.currentRepeatType
            );
        }
        else {
            semRepeatToolbar.classList.add("d-none");
            semRepeatSurrElems.classList.add("d-none");

            semRepeatToolbar.querySelectorAll("button")
                .forEach(btn => {
                    btn.classList.remove("btn-outline-primary");
                    btn.classList.add("btn-outline-secondary");
                });
            semRepeatSurrElems.querySelectorAll(".repeat-surr-elems")
                .forEach(div => {
                    if (!div.classList.contains("d-none")) {
                        div.classList.add("d-none");
                    }
                });
        }

        const isStretchable = SemStretcher.isSemStretchable(selectedElement);
        const semStretchSurrElems = document
            .querySelector(this.htmlSemStretchSurrElemsId);
        if (isStretchable
            && (!this.selectedSemType || this.selectedSemType === "stretch")) {
            surrElemsDiv.classList.remove("d-none");
            semStretchSurrElems.classList.remove("d-none");
            if (!this.selectedSemType) {
                this.selectedSemType = "stretch";
            }

            const {
                horizontalFactor,
                verticalFactor
            } = SemStretcher.getSemStretchableConfigOfElement(selectedElement);

            const horizontalFactorNumInput = semStretchSurrElems
                .querySelector("#stretch-horizontal-num-input");
            const verticalFactorNumInput = semStretchSurrElems
                .querySelector("#stretch-vertical-num-input");
            horizontalFactorNumInput.value = horizontalFactor;
            verticalFactorNumInput.value = verticalFactor;
        }
        else {
            semStretchSurrElems.classList.add("d-none");
        }

        const isAnchorable = SemAnchorer.isSemAnchorable(selectedElement);
        const semAnchorSurrElems = document
            .querySelector(this.htmlSemAnchorSurrElemsId);
        if (isAnchorable
            && (!this.selectedSemType || this.selectedSemType === "anchor")) {
            surrElemsDiv.classList.remove("d-none");
            semAnchorSurrElems.classList.remove("d-none");
            if (!this.selectedSemType) {
                this.selectedSemType = "anchor";
            }
        }
        else {
            semAnchorSurrElems.classList.add("d-none");
        }

        if (!isRepeatable && !isStretchable && !isAnchorable) {
            surrElemsDiv.classList.add("d-none");
        }

        this.updatePositions();
    }

    initSurrElemsOfRepeatTypeWithMetadata(selectedElement, repeatType) {
        const {
            directions,
            repeatSymmetrically,
            path,
            rotateAlongPath,
            amount,
            margin
        } = SemRepeater.getSemRepeatableConfigOfElement(selectedElement);

        switch (repeatType) {
        case "directions":
            this.initRepeatDirectionsSurrElemsWithMetadata(directions, amount);
            break;
        case "grid":
            this.initRepeatGridSurrElemsWithMetadata(
                amount,
                repeatSymmetrically
            );
            break;
        case "direction":
            this.initRepeatDirectionSurrElemsWithMetadata(amount);
            break;
        case "path":
            this.initRepeatPathSurrElemsWithMetadata(path, rotateAlongPath);
            break;
        default:
            break;
        }
    }

    initRepeatDirectionsSurrElemsWithMetadata(directions, amountMap) {
        const dirBtns = document.querySelector(this.htmlSurrRepeatDirsElemsId)
            .querySelectorAll(".repeat-direction-btn");
        const dirsNumInputsDiv = document
            .querySelector(this.htmlRepeatDirsNumInputsId);
        dirBtns.forEach(btn => {
            const repeatVec = SemRepeater.vectorMap.get(
                btn.dataset.repeatDirection
            );
            let index = -1;
            if (directions !== undefined && directions.length > 0) {
                index = directions.findIndex(
                    value => repeatVec.x === value.x && repeatVec.y === value.y
                );
            }

            const numInputOfDir = dirsNumInputsDiv
                .querySelector(
                    `[data-repeat-direction="${btn.dataset.repeatDirection}"]`
                );
            const keyOfAmountOfDir = SemRepeater
                .getDirKeyInAmountMap(amountMap, repeatVec);
            const amountOfDir = amountMap.get(keyOfAmountOfDir);

            if (index > -1) {
                btn.classList.replace(
                    "btn-outline-secondary",
                    "btn-outline-primary"
                );
                numInputOfDir.classList.remove("d-none");
                numInputOfDir.value = amountOfDir;

                if (dirsNumInputsDiv.classList.contains("d-none")) {
                    dirsNumInputsDiv.classList.remove("d-none");
                }
            }
            else {
                numInputOfDir.value = amountOfDir;
                btn.classList.replace(
                    "btn-outline-primary",
                    "btn-outline-secondary"
                );
                numInputOfDir.classList.add("d-none");
            }
        });

        if (dirsNumInputsDiv.querySelectorAll(".d-none").length === 8) {
            dirsNumInputsDiv.classList.add("d-none");
        }
    }

    initRepeatGridSurrElemsWithMetadata(amountMap, repeatSymmetrically) {
        const gridNumInputs = document
            .querySelector(this.htmlSurrRepeatGridElemsId)
            .querySelectorAll("num-input");
        gridNumInputs.forEach(numInput => {
            const repeatVec = SemRepeater.vectorMap.get(
                numInput.dataset.repeatDirection
            );
            const keyOfAmountOfDir = SemRepeater
                .getDirKeyInAmountMap(amountMap, repeatVec);
            numInput.value = amountMap.get(keyOfAmountOfDir);
        });

        const repeatGridSurrElemsDiv = document
            .querySelector(this.htmlSurrRepeatGridElemsId);
        const gridSymmetryToggle = repeatGridSurrElemsDiv
            .querySelector("#repeat-grid-symmetry-toggle");
        const asymmetricRepeatInputFields = repeatGridSurrElemsDiv
            .querySelectorAll(".asymmetric-repeat-amount");
        if (repeatSymmetrically) {
            gridSymmetryToggle.classList.replace(
                "btn-outline-secondary",
                "btn-outline-primary"
            );
            asymmetricRepeatInputFields.forEach(
                field => field.classList.add("d-none")
            );
            gridSymmetryToggle.dataset.repeatSymmetrically = "true";
        }
        else {
            gridSymmetryToggle.classList.replace(
                "btn-outline-primary",
                "btn-outline-secondary"
            );
            asymmetricRepeatInputFields.forEach(
                field => field.classList.remove("d-none")
            );
            gridSymmetryToggle.dataset.repeatSymmetrically = "false";
        }
    }

    initRepeatDirectionSurrElemsWithMetadata(amount) {
        const customDirs = Array.from(amount.keys())
            .filter(dir => dir.x > -1 && dir.x < 0
            || dir.x > 0 && dir.x < 1
            || dir.y > -1 && dir.y < 0
            || dir.y > 0 && dir.y < 1);
        const customDirsNumInputsDiv = document
            .querySelector("#repeat-direction-surr-elems");
        const customDirsNumInputs = customDirsNumInputsDiv
            .querySelectorAll("num-input");
        customDirsNumInputs.forEach(numInput => {
            if (customDirs.find(dir => dir.x === numInput.dataset.customDirX
                && dir.y === numInput.dataset.customDirY) === undefined) {
                numInput.remove();
            }
        });
        if (customDirs !== undefined && customDirs.length > 0) {
            customDirs.forEach(dir => {
                const dirInput = customDirsNumInputsDiv
                    .querySelector(
                        `[data-custom-dir-x="${dir.x}"][data-custom-dir-y="${dir.y}"]`
                    );
                if (dirInput) {
                    dirInput.value = amount.get(dir);
                }
                else {
                    this.addCustomDirDivAndInput(amount, dir);
                }
            });
        }
    }

    addCustomDirDivAndInput(amount, dir) {
        const customDirsNumInputsDiv = document
            .querySelector("#repeat-direction-surr-elems");
        const offsetFromRight = 50 - dir.x * 50;
        const offsetFromLeft = 50 + dir.x * 50;
        customDirsNumInputsDiv.insertAdjacentHTML(
            "beforeend",
            `
                            <div style="position: absolute; 
                            top: ${50 + dir.y * 50}%; 
                            ${dir.x > 0 ? "right" : "left"}: 
                            ${dir.x > 0 ? offsetFromRight : offsetFromLeft}%;
                            transform:
                            translate(${dir.x > 0 ? 50 : -50}%, -50%); 
                            pointer-events: auto">
                                <num-input 
                                data-custom-dir-x="${dir.x}" 
                                data-custom-dir-y="${dir.y}">
                                </num-input>
                            </div>`
        );
        const newDirInput = customDirsNumInputsDiv
            .querySelector(
                `num-input[data-custom-dir-x="${dir.x}"][data-custom-dir-y="${dir.y}"]`
            );
        const dirKey = SemRepeater.getDirKeyInAmountMap(amount, dir);
        if (dirKey) {
            newDirInput.value = amount.get(dirKey);
        }
        newDirInput.addEventListener("changed", evt => {
            SemRepeater.updateAmountOfDir(
                this.selectedElement,
                dir,
                Number.parseInt(newDirInput.value, 10)
            );
        });
    }

    initRepeatPathSurrElemsWithMetadata(path, rotateAlongPath) {
        const repeatPathRotationToggle = document
            .querySelector(this.htmlRepeatPathRotationToggleId);
        repeatPathRotationToggle.dataset.repeatAlongPath = rotateAlongPath;
        if (rotateAlongPath) {
            repeatPathRotationToggle.classList.remove("btn-outline-secondary");
            repeatPathRotationToggle.classList.add("btn-outline-primary");
        }
        else {
            repeatPathRotationToggle.classList.add("btn-outline-secondary");
            repeatPathRotationToggle.classList.remove("btn-outline-primary");
        }
    }

    updatePositions() {
        switch (this.selectedSemType) {
        case "repeat": {
            if (this.currentRepeatType !== null) {
                const currSurrElems = document
                    .querySelector(this.htmlSemRepeatSurrElemsId)
                    .querySelector(
                        `[data-repeat-type="${this.currentRepeatType}"]`
                    );
                this.positionElementsAroundElement(
                    this.selectedElement,
                    parseInt(currSurrElems.dataset.overflowWidth, 10),
                    parseInt(currSurrElems.dataset.overflowHeight, 10)
                );
            }
            break;
        }
        case "stretch": {
            const stretchSurrElemsDiv = document
                .querySelector(this.htmlSemStretchSurrElemsId);
            this.positionElementsAroundElement(
                this.selectedElement,
                parseInt(stretchSurrElemsDiv.dataset.overflowWidth, 10),
                parseInt(stretchSurrElemsDiv.dataset.overflowHeight, 10)
            );
            break;
        }
        case "anchor": {
            const anchorSurrElemsDiv = document
                .querySelector(this.htmlSemAnchorSurrElemsId);
            this.positionElementsAroundElement(
                this.selectedElement,
                parseInt(anchorSurrElemsDiv.dataset.overflowWidth, 10),
                parseInt(anchorSurrElemsDiv.dataset.overflowHeight, 10)
            );
            break;
        }
        default:
            break;
        }

        this.updateToolbarsPosition();
    }

    updateToolbarsPosition() {
        this.positionToolbarsOverElement(this.selectedElement);
    }

    // this has to be called when menu is visible
    // because div has no dimensions otherwise
    positionToolbarsOverElement(selectedElement) {
        const surroundingElementsDiv = document
            .querySelector(this.htmlPopupSurroundingElementsId);
        const surrElemDivRect = surroundingElementsDiv.getBoundingClientRect();

        const eleRect = selectedElement.node.getBoundingClientRect();
        let posX = eleRect.x + eleRect.width / 2 + window.pageXOffset;
        let posY = eleRect.y - surrElemDivRect.height / 2 + window.pageYOffset;
        if (surrElemDivRect.height !== 0) {
            posY += eleRect.height / 2;
        }

        const toolbarsDiv = document.querySelector(this.htmlPopupToolbarsId);

        const popupRect = toolbarsDiv.getBoundingClientRect();
        posX -= popupRect.width / 2;
        posY -= popupRect.height + this.verticalOffset;

        toolbarsDiv.style.top = `${posY}px`;
        toolbarsDiv.style.left = `${posX}px`;
    }

    positionElementsAroundElement(
        selectedElement,
        overflowWidth,
        overflowHeight
    ) {
        const eleRect = selectedElement.node.getBoundingClientRect();
        let posX = eleRect.x + eleRect.width / 2 + window.pageXOffset;
        let posY = eleRect.y + eleRect.height / 2 + window.pageYOffset;

        const elementsDiv = document
            .querySelector(this.htmlPopupSurroundingElementsId);

        elementsDiv.style.width = `${eleRect.width + overflowWidth}px`;
        elementsDiv.style.height = `${eleRect.height + overflowHeight}px`;

        const elementsRect = elementsDiv.getBoundingClientRect();
        posX -= elementsRect.width / 2;
        posY -= elementsRect.height / 2;

        elementsDiv.style.top = `${posY}px`;
        elementsDiv.style.left = `${posX}px`;
    }

    selectSemType(semType) {
        switch (semType) {
        case "repeat":
            if (this.selectedElement.isGeoScalable()) {
                this.selectedElement.geoScalable(false);
            }
            SemRepeater.addRepeatableBehaviour(this.selectedElement);
            document.querySelector(this.htmlSemRepeatToolbarId)
                .classList.remove("d-none");
            this.selectedSemType = semType;
            break;
        case "stretch":
            if (this.selectedElement.isGeoScalable()) {
                this.selectedElement.geoScalable(false);
            }
            SemStretcher.addStretchableBehaviour(this.selectedElement);
            document.querySelector(this.htmlSemRepeatToolbarId)
                .classList.add("d-none");
            this.selectedSemType = semType;
            break;
        case "anchor":
            if (this.selectedElement.isGeoScalable()) {
                this.selectedElement.geoScalable(false);
            }
            if (SemStretcher.isSemStretchable(this.selectedElement)) {
                SemStretcher.updateUpdatePos(
                    this.selectedElement, false
                );
            }
            SemAnchorer.addAnchoableBehabiour(this.selectedElement);
            SemAnchorer.activateAddingOfAnchorPoint(this.selectedElement);
            document.querySelector(this.htmlSemRepeatToolbarId)
                .classList.add("d-none");
            this.selectedSemType = semType;
            break;
        default:
            break;
        }

        const surrElemsDiv = document
            .querySelector(this.htmlPopupSurroundingElementsId);
        const typeSpecificSurrElemsDivs = surrElemsDiv
            .querySelectorAll(".popup-surr-elements-of-sem-type");
        typeSpecificSurrElemsDivs.forEach(div => {
            if (div.dataset.semType === semType) {
                if (surrElemsDiv.classList.contains("d-none")) {
                    surrElemsDiv.classList.remove("d-none");
                }
                div.classList.remove("d-none");
            }
            else {
                div.classList.add("d-none");
            }
        });

        this.updatePositions();
    }

    onRepeatTypeChanged(repeatType) {
        const repeatToolbar = document
            .querySelector(this.htmlSemRepeatToolbarId);
        const semRepeatSurrElems = document
            .querySelector(this.htmlSemRepeatSurrElemsId);
        if (this.currentRepeatType !== null) {
            repeatToolbar
                .querySelector(
                    `[data-repeat-type="${this.currentRepeatType}"]`
                )
                .classList
                .replace(
                    "btn-outline-primary",
                    "btn-outline-secondary"
                );

            semRepeatSurrElems
                .querySelector(`[data-repeat-type="${this.currentRepeatType}"]`)
                .classList.add("d-none");
        }

        this.selectedRepeatType = repeatType;

        repeatToolbar
            .querySelector(`[data-repeat-type="${repeatType}"]`)
            .classList
            .replace("btn-outline-secondary", "btn-outline-primary");

        document.querySelector(this.htmlPopupSurroundingElementsId)
            .classList.remove("d-none");
        semRepeatSurrElems.classList.remove("d-none");
        const typesSurrElems = semRepeatSurrElems
            .querySelector(`[data-repeat-type="${repeatType}"]`);
        typesSurrElems.classList.remove("d-none");

        switch (repeatType) {
        case "directions":
            SemRepeater.activateRepeatTypeDirections(this.selectedElement);
            break;
        case "grid":
            SemRepeater.activateRepeatTypeGrid(this.selectedElement);
            break;
        case "direction":
            SemRepeater
                .activateRepeatTypeCustomDirections(this.selectedElement);
            SemRepeater.activateAddingOfCustomDirection(this.selectedElement);
            break;
        case "path":
            SemRepeater.activateRepeatTypePath(this.selectedElement);
            SemRepeater.activateAddingOfPath(this.selectedElement);
            break;
        default:
            break;
        }
    }
}
