export default class ObserverHandler {
    constructor(el = null, onChange = null) {
        this.el = el;
        this.onChange = onChange;
        this.observerInst = null;
        this.isObserving = false;
    }

    addObserver(el = null, onChange = null) {
        if (!el && !this.el) {
            console.log("Cannot add observer to null! "
                + "An element needs to be defined.");
            return;
        }
        if (!onChange && !this.onChange) {
            console.log("No onChange method given. Observer is not added.");
            return;
        }
        if (this.el && this.isObserving) {
            this.removeObserver();
        }
        if (el && el !== this.el) {
            this.el = el;
        }
        if (onChange && onChange !== this.onChange) {
            this.onChange = onChange;
        }

        if (MutationObserver) {
            this.observerInst = this.observerInst
                || new MutationObserver(() => {
                    this.onChange();
                });
            this.observerInst.observe(
                this.el.node,
                { attributes: true }
            );
        }
        else {
            this.el.off("DOMAttrModified.select");

            this.el.on("DOMAttrModified.select", () => {
                this.onChange();
            });
        }

        this.isObserving = true;
    }

    removeObserver() {
        if (!this.isObserving) return;

        if (MutationObserver) {
            try {
                this.observerInst.disconnect();
                delete this.observerInst;
            }
            catch (e) {
                console.log("Disconnecting observer failed!");
            }
        }
        else {
            this.el.off("DOMAttrModified.select");
        }

        this.isObserving = false;
    }
}
