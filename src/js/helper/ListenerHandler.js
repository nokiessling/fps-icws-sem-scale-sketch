export default class ListenerHandler {
    constructor(
        owner,
        defaultTarget,
        defaultAddListenerFuncName,
        defaultRemoveListenerFuncName
    ) {
        this.owner = owner;
        this.defaultTarget = defaultTarget;
        this.attatchedListeners = new Map();
        this.attatchedListeners.set(this.defaultTarget, []);
        this.targetsAddListenerFuncNames = new Map([
            [defaultTarget, defaultAddListenerFuncName]
        ]);
        this.targetsRemoveListenerFuncNames = new Map([
            [defaultTarget, defaultRemoveListenerFuncName]
        ]);
    }

    addTarget(target, addListenerFuncName, removeListenerFuncName) {
        this.attatchedListeners.set(target, []);
        this.targetsAddListenerFuncNames.set(target, addListenerFuncName);
        this.targetsRemoveListenerFuncNames.set(target, removeListenerFuncName);
    }

    on(type,
        func,
        target = this.defaultTarget) {
        const boundFunc = func.bind(this.owner);
        if (this.attatchedListeners.has(target)) {
            const targetsListeners = this.attatchedListeners.get(target);
            targetsListeners.push({ type, boundFunc });
            this.attatchedListeners.set(target, targetsListeners);
        }
        else {
            this.attatchedListeners.set(target, [{ type, boundFunc }]);
        }
        if (!this.targetsAddListenerFuncNames.has(target)) {
            console.log("Error: can not add listener, unknown target");
            console.trace();
            return;
        }
        const addListenerFuncName = this.targetsAddListenerFuncNames
            .get(target);
        target[addListenerFuncName](type, boundFunc);
    }

    removeListener(type, target = this.defaultTarget) {
        const listenersOfTarget = this.attatchedListeners.get(target);
        const listener = listenersOfTarget.find(
            l => type === l.type
        );

        if (listener) {
            const { type, boundFunc } = listener;
            if (!this.targetsRemoveListenerFuncNames.has(target)) {
                console.log("Error: can not remove listener, unknown target");
                return;
            }
            const removeListenerFuncName = this.targetsRemoveListenerFuncNames
                .get(target);
            target[removeListenerFuncName](type, boundFunc);
            const idx = listenersOfTarget.indexOf(listener);
            listenersOfTarget.splice(idx, 1);
            this.attatchedListeners.set(target, listenersOfTarget);
        }
    }

    removeTarget(target) {
        this.removeAllListenersOfTarget(target);
        this.attatchedListeners.delete(target);
        this.targetsAddListenerFuncNames.delete(target);
        this.targetsRemoveListenerFuncNames.delete(target);
    }

    removeAllListeners() {
        Array.from(this.attatchedListeners.keys()).forEach(
            target => {
                this.removeAllListenersOfTarget(target);
            }
        );
    }

    removeAllListenersOfTarget(target) {
        const removeListenerFuncName = this.targetsRemoveListenerFuncNames
            .get(target);
        const listeners = this.attatchedListeners.get(target);
        if (listeners === undefined) {
            return;
        }
        listeners.forEach(({ type, boundFunc }) => {
            target[removeListenerFuncName](type, boundFunc);
        });
        this.attatchedListeners.set(target, []);
    }
}
