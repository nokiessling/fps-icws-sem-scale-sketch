import { SVG } from "@svgdotjs/svg.js";
import "@svgdotjs/svg.draggable.js";
import "svg.select.js/src/svg.select";
import "svg.resize.js/src/svg.resize";
import "jstree";

import "./styles/index.css";
import "bootstrap";

import "./js/svgjs/semantic_scale/SemRepeatable";
import "./js/svgjs/semantic_scale/SemStretchable";
import "./js/svgjs/semantic_scale/SemAnchorable";
import "./js/svgjs/basic_element_behaviour/GeoScalable";
import "./js/svgjs/basic_element_behaviour/MoveWithParentOnDrag";
import "./js/svgjs/selection/EasierSelectable";
import "./js/svgjs/selection/Highlighter";
import FreeLineTool from "./js/svgjs/tools/FreeLineTool";
import StraightLineTool from "./js/svgjs/tools/StraightLineTool";
import ShapesTool, { ShapesEnum } from "./js/svgjs/tools/ShapesTool";
import PathTool from "./js/svgjs/tools/PathTool";
import ToolManager from "./js/svgjs/tools/ToolManager";
import SvgHierarchyManager from "./js/svgjs/hierarchy/SvgHierarchyManager";
import SvgSelectionManager from "./js/svgjs/selection/SvgSelectionManager";
import Inspector from "./js/svgjs/inspector/Inspector";
import Exporter from "./js/svgjs/import_export/Exporter";
import SemScalePopupMenu from "./js/svgjs/menu/SemScalePopupMenu";
import NumInput from "./js/svgjs/menu/NumInput";
import CustomDirLine from "./js/svgjs/inspector/CustomDirLine";
import DrawingMode from "./js/svgjs/modes/DrawingMode";
import SelectingMode from "./js/svgjs/modes/SelectingMode";
import ScalingMode from "./js/svgjs/modes/ScalingMode";
import DeletingMode from "./js/svgjs/modes/DeletingMode";
import ModeManager from "./js/svgjs/modes/ModeManager";

let draw = SVG()
    .addTo("#root")
    .size("100%", "100%");

let svgEle = document.querySelector("svg");

SvgHierarchyManager.Instance = new SvgHierarchyManager(draw);

const drawToolsSidebar = document.querySelector("#drawToolbarVertical");
const defaultToolBtn = drawToolsSidebar.querySelector("button");
const defaultTool = defaultToolBtn.dataset.toolType;
let activatedToolButton = defaultToolBtn;

const shapesDropdown = document.querySelector("#drawToolbarShapesDropdown");
const shapesDropdownValuesDiv = document.querySelector(
    "[aria-labelledby=\"drawToolbarShapesDropdown\"]"
);
const defaultShapeBtn = shapesDropdownValuesDiv.querySelector("button");
const defaultShape = defaultShapeBtn.dataset.shapeType;
shapesDropdown.innerHTML = defaultShapeBtn.innerHTML;

const tools = new Map();
tools.set("freeLine", new FreeLineTool(svgEle, draw));
tools.set("straightLine", new StraightLineTool(svgEle, draw));
tools.set("shapes", new ShapesTool(
    svgEle,
    draw,
    ShapesEnum[defaultShape]
));
tools.set("path", new PathTool(svgEle, draw));

ToolManager.Instance = new ToolManager(tools, defaultTool);

drawToolsSidebar
    .querySelectorAll("button")
    .forEach(value => {
        if (value.dataset.toolType === defaultTool) {
            value.classList.replace("btn-secondary", "btn-primary");
        }
        else {
            value.classList.replace("btn-primary", "btn-secondary");
        }
        value.addEventListener("click", ev => {
            if (value !== activatedToolButton
                && value.dataset.toolType !== undefined) {
                activatedToolButton.classList
                    .replace("btn-primary", "btn-secondary");
                value.classList
                    .replace("btn-secondary", "btn-primary");
                activatedToolButton = value;
                ToolManager.Instance.selectTool(value.dataset.toolType);
            }
        });
    });

shapesDropdownValuesDiv
    .querySelectorAll("button")
    .forEach(value => {
        value.addEventListener("click", ev => {
            shapesDropdown.innerHTML = value.innerHTML;
            ToolManager.Instance.selectedTool.setshape(
                ShapesEnum[value.dataset.shapeType]
            );
        });
    });

const semScalePopupMenu = new SemScalePopupMenu();

const modes = new Map();
modes.set("drawing", new DrawingMode("#drawToolbarVertical"));
modes.set("selecting", new SelectingMode(semScalePopupMenu));
modes.set("scaling", new ScalingMode(semScalePopupMenu));
modes.set("deleting", new DeletingMode());

const modesDiv = document.querySelector("#toolbar-modes");
const defaultModeBtn = modesDiv.querySelector("button");
const defaultMode = defaultModeBtn.dataset.mode;
let activatedModeButton = defaultModeBtn;

const modeManager = new ModeManager(modes, defaultMode);

modesDiv
    .querySelectorAll("button")
    .forEach(value => {
        if (value.dataset.mode === defaultMode) {
            value.classList.replace("btn-secondary", "btn-primary");
        }
        else {
            value.classList.replace("btn-primary", "btn-secondary");
        }
        value.addEventListener("click", ev => {
            if (value !== activatedModeButton
                && value.dataset.mode !== undefined) {
                activatedModeButton.classList
                    .replace("btn-primary", "btn-secondary");
                value.classList
                    .replace("btn-secondary", "btn-primary");
                activatedModeButton = value;
                modeManager.selectMode(value.dataset.mode);
            }
        });
    });

const inspector = new Inspector();
const exporter = new Exporter();

const initNewDrawing = () => {
    if (svgEle !== null) {
        svgEle.remove();
    }
    document.querySelector("#sem-behaviour-marker").innerHTML = "";

    draw = SVG()
        .addTo("#root")
        .size("100%", "100%");

    svgEle = document.querySelector("svg");

    SvgSelectionManager.Instance.reset();

    SvgHierarchyManager.Instance.reset(draw);

    ToolManager.Instance.reset(draw, svgEle);
    activatedToolButton = defaultToolBtn;
    drawToolsSidebar
        .querySelectorAll("button")
        .forEach(value => {
            if (value.dataset.toolType === defaultTool) {
                value.classList.replace("btn-secondary", "btn-primary");
            }
            else {
                value.classList.replace("btn-primary", "btn-secondary");
            }
        });
    shapesDropdown.innerHTML = defaultShapeBtn.innerHTML;

    semScalePopupMenu.deactivate();

    modeManager.reset();
    activatedModeButton = defaultModeBtn;
    modesDiv
        .querySelectorAll("button")
        .forEach(value => {
            if (value.dataset.mode === defaultMode) {
                value.classList.replace("btn-secondary", "btn-primary");
            }
            else {
                value.classList.replace("btn-primary", "btn-secondary");
            }
        });

    inspector.reset();
};

document.querySelector("#toolbar-new-sketch")
    .addEventListener("click", evt => {
        initNewDrawing();
    });

